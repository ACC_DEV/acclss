package accsarathi.orimark.com.acclss.Util;

/**
 * Created by prakash on 02/08/17.
 */

public class AllLocation {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String name;
    private String id;

    public AllLocation(String id, String name){
        this.id=id;
        this.name=name;
    }

    public String toString(){
        return name;
    }
}
