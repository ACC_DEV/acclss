package accsarathi.orimark.com.acclss.Model;

/**
 * Created by prakash on 1/31/18.
 */

public class QuestionAnsModel {

    public String getQueID() {
        return queID;
    }

    public void setQueID(String queID) {
        this.queID = queID;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    private String queID;
    private String answer;
    private String comments;

    public QuestionAnsModel(String queID,String answer,String comments){
        this.queID=queID;
        this.answer=answer;
        this.comments=comments;
    }
}
