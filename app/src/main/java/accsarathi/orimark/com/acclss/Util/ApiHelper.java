package accsarathi.orimark.com.acclss.Util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import accsarathi.orimark.com.acclss.R;


public class ApiHelper extends AsyncTask<String, String, Object> {

    ServiceHandler jsonParser = new ServiceHandler();
    ProgressDialog dialog;
    String userid_, login_code, token_id;
    Context context;
    String callReference;
    private String valname, valemail, valdepartment, valphone, valpassword, unique_id;
    private String login_number, login_password;
    boolean showDialog_;
    private String filtername, checklistID, locationId;
    private String data1, data2, data3, data4, data5;
    private String vechiclePassportNo;

    public ApiHelper(Context context, String callReference, String login_number, String device_id, String token_id, TaskDelegate delegate) {
        if (callReference.equals(AllStaticVariables.apiAction.signin)) {
            this.context = context;
            this.callReference = callReference;
            this.login_number = login_number;
            this.token_id = token_id;
            this.unique_id = device_id;
            this.delegate = delegate;
        }
        /*this.context = context;
        this.callReference = callReference;
        this.token_id = token_id;
        this.userid_ = userid_;
        this.login_code = login_code;
        this.delegate = delegate;*/
    }

    public ApiHelper(Context context, String callReference, TaskDelegate delegate) {
        this.context = context;
        this.callReference = callReference;
        this.delegate = delegate;

    }

    public ApiHelper(Context context, String callReference, String checklistID, TaskDelegate delegate) {
        this.context = context;
        this.callReference = callReference;
        if (AllStaticVariables.apiAction.chkVechiclePssprt.equalsIgnoreCase(callReference)) {
            this.vechiclePassportNo = checklistID;
        } else {
            this.checklistID = checklistID;
        }
        if (AllStaticVariables.apiAction.acclssrepairevehicle.equalsIgnoreCase(callReference)) {
            this.userid_ = checklistID;
        }

        this.delegate = delegate;

    }



    private TaskDelegate delegate;

    public interface TaskDelegate {

        void onTaskFisnishGettingData(Object result);
    }

    @Override
    protected void onPreExecute() {
        if (AllStaticVariables.apiAction.signin.equalsIgnoreCase(callReference)) {
            showDialog_ = true;
            dialog = ProgressDialog.show(context, "", context.getString(R.string.loading_signin_));
            dialog.show();
        } else if (AllStaticVariables.apiAction.reject_list.equalsIgnoreCase(callReference)
                || AllStaticVariables.apiAction.repair_list.equalsIgnoreCase(callReference)
                || AllStaticVariables.apiAction.allowed_list.equalsIgnoreCase(callReference)
                || AllStaticVariables.apiAction.getSaveCheckList.equalsIgnoreCase(callReference)
                || AllStaticVariables.apiAction.acclssrepairevehicle.equalsIgnoreCase(callReference)) {
            showDialog_ = true;
            dialog = ProgressDialog.show(context, "", context.getString(R.string.loading_));
            dialog.show();
        } else if (AllStaticVariables.apiAction.chkVechiclePssprt.equalsIgnoreCase(callReference)) {
            showDialog_ = true;
            dialog = ProgressDialog.show(context, "", context.getString(R.string.otp_verifiy));
            dialog.show();
        }

    }

    protected Object doInBackground(String... args) {
        String response = null;
        JSONObject jsonObject = null;
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            if (AllStaticVariables.apiAction.signin.equalsIgnoreCase(callReference)) {
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.loginVariable.action, "acclsslogin"));
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.loginVariable.mobile, login_number));
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.loginVariable.device_id, unique_id));
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.loginVariable.device_type, AllStaticVariables.loginVariable.Android));
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.loginVariable.token_id, token_id));

                response = jsonParser.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST, nameValuePairs);

            } else if (AllStaticVariables.apiAction.reject_list.equalsIgnoreCase(callReference)) {
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.loginVariable.action, "acclssrejectedchecklist"));
                response = jsonParser.makeServiceCall(MyUrl.baseAPI, ServiceHandler.GET, nameValuePairs);
            } else if (AllStaticVariables.apiAction.allowed_list.equalsIgnoreCase(callReference)) {
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.loginVariable.action, "acclssallowedchecklist"));
                response = jsonParser.makeServiceCall(MyUrl.baseAPI, ServiceHandler.GET, nameValuePairs);
            } else if (AllStaticVariables.apiAction.repair_list.equalsIgnoreCase(callReference)) {
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.loginVariable.action, "acclssrepairedchecklist"));
                response = jsonParser.makeServiceCall(MyUrl.baseAPI, ServiceHandler.GET, nameValuePairs);
            }else if (AllStaticVariables.apiAction.getSaveCheckList.equalsIgnoreCase(callReference)) {
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.loginVariable.action, "acclssidbychecklist"));
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.checklist_id, checklistID));
                response = jsonParser.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST, nameValuePairs);
            } else if (AllStaticVariables.apiAction.chkVechiclePssprt.equalsIgnoreCase(callReference)) {
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.loginVariable.action, "acclssvehiclemovechecklist"));
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.vpassportno, vechiclePassportNo));
                response = jsonParser.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST, nameValuePairs);
            }else if (AllStaticVariables.apiAction.acclssrepairevehicle.equalsIgnoreCase(callReference)) {
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.loginVariable.action, "acclssrepairevehicle"));
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.userid, userid_));
                response = jsonParser.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST, nameValuePairs);
            }
            jsonObject = new JSONObject(response);
            System.out.println("nameValuePairs ----- " + nameValuePairs);
            Log.e("response", response);
            return jsonObject;

        } catch (Exception e1) {
            System.out.println(e1);
            return null;
        }
    }

    protected void onPostExecute(Object result) {
        if (showDialog_)
            dialog.dismiss();
        if (delegate != null)
            delegate.onTaskFisnishGettingData(result);
    }
}
