package accsarathi.orimark.com.acclss.Util;

/**
 * Created by prakash on 04/08/17.
 */

public class CouponDeals {

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getDownload_button() {
        return download_button;
    }

    public void setDownload_button(String download_button) {
        this.download_button = download_button;
    }

    public String getAttchment() {
        return attchment;
    }

    public void setAttchment(String attchment) {
        this.attchment = attchment;
    }

    private String image, subject, message, type, company_id, company_name, download_button, attchment;

    public CouponDeals(String type, String company_id, String company_name, String message,
                       String subject, String image, String download_button, String attchment){
        this.type=type;
        this.company_id=company_id;
        this.company_name=company_name;
        this.message=message;
        this.subject=subject;
        this.image=image;
        this.download_button=download_button;
        this.attchment=attchment;

    }
}
