package accsarathi.orimark.com.acclss.Activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.MyUrl;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;
import accsarathi.orimark.com.acclss.Util.ServiceHandler;

/**
 * Created by prakash on 24/9/16.
 */
public class OTPActivity extends AppCompatActivity {
    EditText input_otp;
    Button btn_verify;
    //Internet Service
    NetworkConnection nw;
    ProgressDialog prgDialog;
    Boolean netConnection = false;
    String valOTP, user_id;
    TextInputLayout inputlayout_for_otp;
    private String user_email, user_mobile;
    private TextView resendTv, otpTv;
    Typeface typeFaceBold, typeFaceRegular;
    private TextView timerHint,timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_page_);

        typeFaceBold = Typeface.createFromAsset(this.getAssets(), "fonts/PTC75F_bold.ttf");
        typeFaceRegular = Typeface.createFromAsset(this.getAssets(), "fonts/PTC55F_regular.ttf");

        nw = new NetworkConnection(getApplicationContext());
        prgDialog = new ProgressDialog(this);
        prgDialog.setCancelable(false);

        inputlayout_for_otp = (TextInputLayout) findViewById(R.id.inputlayout_for_otp);
        input_otp = (EditText) findViewById(R.id.input_otp);
        btn_verify = (Button) findViewById(R.id.btn_verify);
        resendTv = (TextView) findViewById(R.id.resendTv);
        resendTv.setPaintFlags(resendTv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        otpTv = (TextView) findViewById(R.id.otpTv);
        input_otp.addTextChangedListener(new MyTextWatcher(input_otp));

        timer = (TextView) findViewById(R.id.timer);
        timer.setVisibility(View.INVISIBLE);

        input_otp.setTypeface(typeFaceRegular);
        btn_verify.setTypeface(typeFaceBold);
        resendTv.setTypeface(typeFaceBold);
        otpTv.setTypeface(typeFaceRegular);

        user_email = MyUrl.getDataFromKey(getApplicationContext(), AllStaticVariables.mySharedpreference.useremail);
        user_mobile = MyUrl.getDataFromKey(getApplicationContext(), AllStaticVariables.mySharedpreference.mobno);
        user_id = MyUrl.getDataFromKey(getApplicationContext(), AllStaticVariables.mySharedpreference.userid);

        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));

        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verifyOTP();
            }
        });
        otpTv.setText("Waiting to automatically detect an SMS sent to "+ user_mobile+"\n"+"Else enter otp manually");
        //otpTv.setText("Please enter the OTP value sent to " + user_mobile);
        resendTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.setVisibility(View.INVISIBLE);
                if (nw.isConnectingToInternet()) {
                    new downloadReOtpOperation().execute();
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                }

            }
        });
        timerCounter();
    }

    private void timerCounter() {
        timer.setVisibility(View.VISIBLE);
        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                String timeRemaining = String.valueOf(millisUntilFinished / 1000);
                timer.setText(timeRemaining);
            }
            public void onFinish() {
                timer.setVisibility(View.INVISIBLE);
            }

        }.start();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                Log.d("Text", message);
                timer.setVisibility(View.GONE);
                input_otp.setText(message);
            }
        }
    };

    private void verifyOTP() {

        if (!validateOTP()) {
            return;
        }
        if (nw.isConnectingToInternet()) {
            new downloadTaskOperation().execute();
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        }

    }

    private boolean validateOTP() {
        valOTP = input_otp.getText().toString().trim();
        if (valOTP.isEmpty()) {
            inputlayout_for_otp.setError(getString(R.string.err_otp));
            requestFocus(input_otp);
            return false;
        } else {
            inputlayout_for_otp.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_otp:
                    validateOTP();
                    break;
            }
        }
    }

    private class downloadTaskOperation extends AsyncTask<String, Void, Void> {

        String status, message;
        JSONObject json2;
        String response;

        @Override
        protected void onPreExecute() {
            prgDialog.setMessage(getResources().getString(R.string.otp_verifiy));
            prgDialog.show();
        }

        @Override
        protected Void doInBackground(String... urls) {


            try {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                ServiceHandler sh = new ServiceHandler();
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.action, AllStaticVariables.apiAction.verifyotp));
                nameValuePairs.add(new BasicNameValuePair("mobile", user_mobile));
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.userid, user_id));
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.otp, valOTP));
                response = sh.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST,
                        nameValuePairs);
                Log.e("response", response);

                JSONObject js = new JSONObject(response);
                json2 = js.getJSONObject(AllStaticVariables.data);
                status = json2.getString(AllStaticVariables.status);
                Log.e("status", status);

                if ("0".equalsIgnoreCase(status)) {
                    message = json2.getString(AllStaticVariables.msg);
                } else {
                    message = json2.getString(AllStaticVariables.msg);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();
            if (status.equalsIgnoreCase("1")) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.verified_successful), Toast.LENGTH_SHORT).show();
                Thread timerThread = new Thread() {
                    public void run() {
                        try {
                            sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.isLogin, "yes");
                            Intent homeIntent = new Intent(OTPActivity.this, HomeslidingActivity.class);
                            startActivity(homeIntent);
                            finish();
                        }
                    }
                };
                timerThread.start();

            } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(result);
        }

    }

    private class downloadReOtpOperation extends AsyncTask<String, Void, Void> {

        String status, message;
        JSONObject json2;
        String response;

        @Override
        protected void onPreExecute() {
            prgDialog.setMessage(getResources().getString(R.string.otp_resend));
            prgDialog.show();
        }

        @Override
        protected Void doInBackground(String... urls) {

            try {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                ServiceHandler sh = new ServiceHandler();
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.action, "acclssresentotp"));
                nameValuePairs.add(new BasicNameValuePair("mobile", user_mobile));
                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.userid, user_id));
                nameValuePairs.add(new BasicNameValuePair("otptype", "1"));

                response = sh.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST,
                        nameValuePairs);
                Log.e("response", response);
                if (response != null) {
                    JSONObject js = new JSONObject(response);
                    json2 = js.getJSONObject(AllStaticVariables.data);
                    status = json2.getString(AllStaticVariables.status);
                    Log.e("status", status);

                    if ("0".equalsIgnoreCase(status)) {
                        message = json2.getString(AllStaticVariables.msg);
                    } else {
                        message = json2.getString(AllStaticVariables.msg);
                    }
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();

            if (status.equalsIgnoreCase("1")) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.sent_otp), Toast.LENGTH_SHORT).show();
                timerCounter();

            } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }

            super.onPostExecute(result);
        }

    }

}