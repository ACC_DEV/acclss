package accsarathi.orimark.com.acclss.Util;

/**
 * Created by prakash on 01/08/17.
 */

public class SelectedRatingData {

    public String getRatingId() {
        return ratingId;
    }

    public void setRatingId(String ratingId) {
        this.ratingId = ratingId;
    }

    public String getRatingPoint() {
        return ratingPoint;
    }

    public void setRatingPoint(String ratingPoint) {
        this.ratingPoint = ratingPoint;
    }

    private String ratingId,ratingPoint;

    public SelectedRatingData(String ratingId, String ratingPoint){

        this.ratingId=ratingId;
        this.ratingPoint=ratingPoint;
    }

}
