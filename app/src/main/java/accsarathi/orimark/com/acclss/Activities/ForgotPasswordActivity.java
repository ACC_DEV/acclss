package accsarathi.orimark.com.acclss.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.MyUrl;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;
import accsarathi.orimark.com.acclss.Util.ServiceHandler;

/**
 * Created by prakash on 24/9/16.
 */
public class ForgotPasswordActivity extends AppCompatActivity {
    EditText emailEt;
    Button btn_send;
    //Internet Service
    NetworkConnection nw;
    ProgressDialog prgDialog;
    Boolean netConnection = false;
    String valemail;
    TextInputLayout inputlayout_registered_email;
    TextView actionbar_title;
    Typeface typeFaceBold, typeFaceRegular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_page);

        nw = new NetworkConnection(getApplicationContext());
        prgDialog = new ProgressDialog(this);
        prgDialog.setCancelable(false);

        //initToolbar();

        typeFaceBold = Typeface.createFromAsset(this.getAssets(), "fonts/PTC75F_bold.ttf");
        typeFaceRegular = Typeface.createFromAsset(this.getAssets(), "fonts/PTC55F_regular.ttf");

        inputlayout_registered_email = (TextInputLayout) findViewById(R.id.inputlayout_registered_email);
        emailEt = (EditText) findViewById(R.id.input_registered_email);
        btn_send = (Button) findViewById(R.id.btn_send);

        emailEt.setTypeface(typeFaceRegular);
        btn_send.setTypeface(typeFaceRegular);

        //emailEt.addTextChangedListener(new MyTextWatcher(emailEt));

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setforgotpassword();
            }
        });

    }


    private void setforgotpassword() {

        if (!validateMobile()) {
            return;
        }
        if (nw.isConnectingToInternet()) {
            new downloadTaskOperation().execute();
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }

   /* private boolean validateRegisteredEmail() {
        valemail = emailEt.getText().toString().trim();
        if (valemail.isEmpty() || !isValidEmail(valemail)) {
            inputlayout_registered_email.setError(getString(R.string.err_registered_email));
            requestFocus(emailEt);
            return false;
        } else {
            inputlayout_registered_email.setErrorEnabled(false);
        }

        return true;
    }
*/


    private boolean validateMobile() {
        valemail = emailEt.getText().toString().trim();
        if (valemail.isEmpty()) {
            inputlayout_registered_email.setError(getString(R.string.err_msg_phone));
            requestFocus(emailEt);
            return false;
        } else if (!isValidPhoneNumber(valemail)) {
            inputlayout_registered_email.setError(getString(R.string.err_msg_phone));
            requestFocus(emailEt);
            return false;
        }

        return true;
    }
    public static boolean isValidPhoneNumber(String phone) {
        boolean check;
        if (phone.length() < 10 || phone.length() > 12) {
            check = false;
        } else {
            check = true;
        }
        return check;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }



    private class downloadTaskOperation extends AsyncTask<String, Void, Void> {

        String status, message;
        JSONObject json2;
        String action;
        String response;

        @Override
        protected void onPreExecute() {
            prgDialog.setMessage("Sending...");

            prgDialog.show();
        }

        @Override
        protected Void doInBackground(String... urls) {


            try {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                ServiceHandler sh = new ServiceHandler();

                nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.action, AllStaticVariables.apiAction.forgetpass));
                nameValuePairs.add(new BasicNameValuePair("mobile", valemail));

                response = sh.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST,
                        nameValuePairs);

                Log.e("response", response);

                JSONObject js = new JSONObject(response);
                json2 = js.getJSONObject("data");
                status = json2.getString("status");
                Log.e("status", status);

                if ("0".equalsIgnoreCase(status)) {
                    message = json2.getString("msg");
                } else {
                    message = json2.getString("msg");
                }
            } catch (Exception ex) {

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();

            if (status.equalsIgnoreCase("1")) {
                apiResponseSuccessDialog(ForgotPasswordActivity.this,valemail);

            } else {
                apiResponseErrorDialog(ForgotPasswordActivity.this,message);
            }
            super.onPostExecute(result);
        }

    }

    public void apiResponseSuccessDialog(final Context context, String mail) {
        AlertDialog.Builder builder1;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder1 = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder1 = new AlertDialog.Builder(context);
        }
        builder1.setTitle("Sent Successfully");
        builder1.setIcon(R.mipmap.ic_success);
        builder1.setMessage("Please wait... We are processing request");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        Intent mIntent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                        finish();
                        startActivity(mIntent);
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void apiResponseErrorDialog(Context context, String message) {
        AlertDialog.Builder builder1;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder1 = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder1 = new AlertDialog.Builder(context);
        }
        builder1.setTitle("Failed");
        builder1.setIcon(R.drawable.cross_img);
        builder1.setMessage(message);
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}