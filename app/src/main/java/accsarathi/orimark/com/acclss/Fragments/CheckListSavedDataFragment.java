package accsarathi.orimark.com.acclss.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import accsarathi.orimark.com.acclss.Activities.HomeslidingActivity;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.MyUrl;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;
import accsarathi.orimark.com.acclss.Util.ServiceHandler;


public class CheckListSavedDataFragment extends Fragment {

    View root;
    ImageView header_image;
    TextView header_title;
    RelativeLayout imageView_containor;
    FragmentManager fragmentManager;
    ListView listView;
    BulkerCheckListAdapter bulkerCheckListAdapter;
    ArrayList<String> bulker_list,bulker_list_qes;
    EditText final_comment,v_regd_edt,driver_name_edt,oper_edt,location_edt;
    LinearLayout approve_lay,con_approve_lay,reject_lay,footer_button_lay;
    Button ok_btn,allow_btn,escalate_btn;
    NetworkConnection nw;
    ProgressDialog prgDialog;
    Boolean netConnection = false;
    String checklist_id,lssrole,lssroleid,plantid,alllowesclatestatus,user_id,location_name,location_id,vehicleid,driverid,vehicleregdno,payloadcapacity,drivername,driverdlno,drivermobileno,transportername,transporterid,transportcontractorsupervisor;
    public static ArrayList<String> selectedAnswers;
    HashMap<Integer,String> selected_ques_ans = new HashMap<>();
    HashMap<Integer,String> selected_ques_ans_comm = new HashMap<>();
    LinearLayout.LayoutParams param_1,param_2;
    public static CheckListSavedDataFragment newInstance() {
        return new CheckListSavedDataFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = null;
        root = inflater.inflate(R.layout.check_list_saved_data_page, container, false);
        nw = new NetworkConnection(getActivity());
        prgDialog = new ProgressDialog(getActivity());
        prgDialog.setCancelable(false);

        prepareData();


        listView =(ListView) root.findViewById(R.id.list);


        View headerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.header_lay, null, false);
        header_title = (TextView)headerView.findViewById(R.id.title);
        imageView_containor = (RelativeLayout) headerView.findViewById(R.id.imageView_containor);
        header_title.setText("Checklist Saved Date");
        header_image =(ImageView)headerView.findViewById(R.id.title_img);
        imageView_containor.setVisibility(View.VISIBLE);
        header_image.setImageResource(R.drawable.blucker_vehicle);
        v_regd_edt =(EditText) headerView.findViewById(R.id.v_regd_edt);
        driver_name_edt =(EditText) headerView.findViewById(R.id.driver_name_edt);
        oper_edt =(EditText) headerView.findViewById(R.id.oper_edt);
        location_edt =(EditText) headerView.findViewById(R.id.location_edt);






        param_1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1.0f
        );
        param_1.setMargins(10, 0, 10, 0);
        param_2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                2.0f
        );
        param_2.setMargins(10, 0, 10, 0);
        //YOUR_VIEW.setLayoutParams(param);



        View footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.checklist_data_view_footer_lay, null, false);
        final_comment = (EditText)footerView.findViewById(R.id.final_comment);
        footer_button_lay = (LinearLayout)footerView.findViewById(R.id.footer_button_lay);
        approve_lay = (LinearLayout)footerView.findViewById(R.id.approve_lay);
        con_approve_lay = (LinearLayout)footerView.findViewById(R.id.con_approve_lay);
        reject_lay = (LinearLayout)footerView.findViewById(R.id.reject_lay);
        ok_btn = (Button) footerView.findViewById(R.id.ok_btn);


        listView.addHeaderView(headerView);
        listView.addFooterView(footerView);

        bulkerCheckListAdapter = new BulkerCheckListAdapter(getActivity(),bulker_list);

        listView.setAdapter(bulkerCheckListAdapter);


        driver_name_edt.setText(drivername);
        v_regd_edt.setText(vehicleregdno);
        location_edt.setText(location_name);
        oper_edt.setText(transportername);

        HomeslidingActivity.menu_icon.setBackgroundResource(R.drawable.left_arrow_50x50);
        HomeslidingActivity.menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectItem(10);
            }
        });

       /* allow_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alllowesclatestatus= "1";

                if(selected_ques_ans.size()==16) {
                    if (nw.isConnectingToInternet() == true) {
                        new downloadReOtpOperation().execute();
                    } else {

                        Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getActivity(), "Please answer all the questions before submit", Toast.LENGTH_LONG).show();
                }
            }
        });
        escalate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alllowesclatestatus ="0";
                if(selected_ques_ans.size()==16) {
                    if (nw.isConnectingToInternet() == true) {
                        new downloadReOtpOperation().execute();
                    } else {

                        Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getActivity(), "Please answer all the questions before submit", Toast.LENGTH_LONG).show();
                }
            }
        });*/

        if (nw.isConnectingToInternet() == true) {
            new downloadCheckListView().execute();
        } else {

            Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        }
        return root;
    }


    public void prepareData(){
        bulker_list_qes = new ArrayList<>();
        bulker_list = new ArrayList<>();

        bulker_list.add("DRIVER");
        bulker_list.add("Driver has valid Driving Licence ");
        bulker_list.add("Driver passed Alcohol Test (Blood Alcohol level)");
        bulker_list.add("Driver has PPE (Safety Helmet, Safety Shoes and Reflective Jacket)");

        bulker_list.add("VEHICLE");
        bulker_list.add("RC Book / Fitness Certificate/ Insurance/Pollution documents available");
        bulker_list.add("Head Lights & Rear lights in working order");
        bulker_list.add("Brakes in working order");
        bulker_list.add("Hand brake in working order");
        bulker_list.add("Windscreen wipers in working order");
        bulker_list.add("All tyres (including spares) in roadworthy condition (treads not worn out)");
        bulker_list.add("No hazardous goods kept inside the Cabin (Stove / Kersone etc.)");



        bulker_list_qes.add("Driver has valid Driving Licence");
        bulker_list_qes.add("Driver passed Alcohol Test (Blood Alcohol level)");
        bulker_list_qes.add("Driver has PPE (Safety Helmet, Safety Shoes and Reflective Jacket)");
        bulker_list_qes.add("RC Book / Fitness Certificate/ Insurance/Pollution documents available");
        bulker_list_qes.add("Head Lights & Rear lights in working order");
        bulker_list_qes.add("Brakes in working order");
        bulker_list_qes.add("Hand brake in working order");
        bulker_list_qes.add("Windscreen wipers in working order");
        bulker_list_qes.add("All tyres (including spares) in roadworthy condition (treads not worn out)");
        bulker_list_qes.add("No hazardous goods kept inside the Cabin (Stove / Kersone etc.)");



    }




    public void selectItem(int position) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (position) {
            case 1:
                fragmentClass = VehiclePermissionFragment.class;
                break;
            default:
                fragmentClass = SafetyAuditFragment.class;
        }
        if (fragmentClass != null) {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }



    public class BulkerCheckListAdapter extends BaseAdapter {

        private Activity activity;
        private LayoutInflater inflater=null;
        ArrayList<String> bulker_list;


        public BulkerCheckListAdapter(Activity a,ArrayList<String> bulker_list) {
            activity = a;
            this.bulker_list = bulker_list;
            selectedAnswers = new ArrayList<>();
            for (int i = 0; i < bulker_list.size(); i++) {
                selectedAnswers.add("Not Attempted");
            }
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            return bulker_list.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi=convertView;
            vi= null;



            if(position==0){
                vi = inflater.inflate(R.layout.bulker_list_inner_header_items, null);
                TextView header_title = (TextView) vi.findViewById(R.id.h_title); // title
                header_title.setText(bulker_list.get(position).toString());

            }else if(position == 4){
                vi = inflater.inflate(R.layout.bulker_list_inner_header_items, null);
                TextView header_title = (TextView) vi.findViewById(R.id.h_title); // title
                header_title.setText(bulker_list.get(position).toString());

            }else {

                vi = inflater.inflate(R.layout.bulker_check_list_item, null);
                ImageView star_img = (ImageView) vi.findViewById(R.id.star_icon);
                TextView questions = (TextView) vi.findViewById(R.id.questions); // title
                RadioButton yes_button = (RadioButton) vi.findViewById(R.id.yes_option);
                RadioButton no_button = (RadioButton) vi.findViewById(R.id.no_option);
                yes_button.setEnabled(false);
                no_button.setEnabled(false);
                final LinearLayout comment_lay = (LinearLayout) vi.findViewById(R.id.comment_lay);
                final EditText comment_edt = (EditText) vi.findViewById(R.id.comment_edt);
                comment_edt.setEnabled(false);
                comment_edt.setFocusable(false);
                questions.setText(bulker_list.get(position).toString());

                if(position >=0 && position <= 11){
                    star_img.setVisibility(View.VISIBLE);
                }else{
                    star_img.setVisibility(View.GONE);
                }
                yes_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                        if(checked){
                            comment_lay.setVisibility(View.GONE);
                            selectedAnswers.set(position,"yes");

                            int index = bulker_list_qes.indexOf(bulker_list.get(position).toString());
                            selected_ques_ans.put(index+1,"yes");
                            selected_ques_ans_comm.put(index+1,"0");
                        }

                        updateButton(getCheckListStatus());

                    }
                });

                no_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                        if(checked){
                            comment_lay.setVisibility(View.VISIBLE);
                            selectedAnswers.set(position,"no");
                            int index = bulker_list_qes.indexOf(bulker_list.get(position).toString());
                            selected_ques_ans.put(index+1,"no");

                        }
                        updateButton(getCheckListStatus());
                    }
                });

                comment_edt.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        int index = bulker_list_qes.indexOf(bulker_list.get(position).toString());
                        selected_ques_ans_comm.put(index+1,s.toString());
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        // TODO Auto-generated method stub
                    }
                });

                if(selectedAnswers.size()!=0){
                    String value = selectedAnswers.get(position).toString();
                    if(value.equalsIgnoreCase("no")){
                        comment_lay.setVisibility(View.VISIBLE);
                        no_button.setChecked(true);

                        int index = bulker_list_qes.indexOf(bulker_list.get(position).toString());
                        //  selected_ques_ans_comm.put(index+1,s.toString());
                        if(selected_ques_ans_comm.containsKey(index+1)){
                            String given_comment = selected_ques_ans_comm.get(index+1).toString();
                            comment_edt.setText(given_comment);
                        }
                    }else if(value.equalsIgnoreCase("yes")){
                        comment_lay.setVisibility(View.GONE);
                        yes_button.setChecked(true);
                    }
                }


            }

            return vi;
        }
    }

    public void updateButton(String value){

        if(value.equalsIgnoreCase("0")){
            reject_lay.setVisibility(View.VISIBLE);
            con_approve_lay.setVisibility(View.GONE);
            approve_lay.setVisibility(View.GONE);

        }else if(value.equalsIgnoreCase("1")){
            reject_lay.setVisibility(View.GONE);
            con_approve_lay.setVisibility(View.GONE);
            approve_lay.setVisibility(View.VISIBLE);

        }else if(value.equalsIgnoreCase("2")){
            reject_lay.setVisibility(View.GONE);
            con_approve_lay.setVisibility(View.VISIBLE);
            approve_lay.setVisibility(View.GONE);

        }


    }




    private class downloadCheckListView extends AsyncTask<String, Void, Void> {

        String status, message;
        JSONObject json2;
        String response,escalatedto,formtype,escalatedtoid,checklistserialno;

        @Override
        protected void onPreExecute() {
            prgDialog.setMessage("Sending...please wait.");
            prgDialog.show();
        }

        @Override
        protected Void doInBackground(String... urls) {

            if (nw.isConnectingToInternet() == true) {
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    ServiceHandler sh = new ServiceHandler();

                    nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.action, "acclssidbychecklist"));
                    nameValuePairs.add(new BasicNameValuePair("checklist_id", checklist_id));

                    response = sh.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST,
                            nameValuePairs);
                    Log.e("response", response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString(AllStaticVariables.status);


                    Log.e("status", status);

                    if ("0".equalsIgnoreCase(status)) {
                        message = js.getString(AllStaticVariables.msg);
                    } else {
                        message = js.getString(AllStaticVariables.msg);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                netConnection = true;
            } else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();

            if (netConnection == false) {
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                if (status.equalsIgnoreCase("1")) {



                } else {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
            super.onPostExecute(result);
        }

    }


    public String getCheckListStatus(){

        String status="";

         for(int i =1;i<=10;i++){

             if(!status.equalsIgnoreCase("reject")) {
                 if (selected_ques_ans.containsKey(i)) {
                     if (selected_ques_ans.get(i).equalsIgnoreCase("yes")) {
                         status = "approve";
                     } else {
                         status = "reject";
                     }
                 }
             }
         }
        if(status.equalsIgnoreCase("approve")) {
            for (int i = 11; i <= 16; i++) {
                if (selected_ques_ans.containsKey(i)) {
                    if (selected_ques_ans.get(i).equalsIgnoreCase("no")) {
                        status = "con_approve";
                    }
                }
            }
        }

        if(status.equalsIgnoreCase("approve")){
            return "1";
        }else if (status.equalsIgnoreCase("reject")){
            return "0";
        }else if(status.equalsIgnoreCase("con_approve")){
            return "2";
        }else{
            return "0";
        }



    }


}
