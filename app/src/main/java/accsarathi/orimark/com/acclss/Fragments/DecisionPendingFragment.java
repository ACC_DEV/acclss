package accsarathi.orimark.com.acclss.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import accsarathi.orimark.com.acclss.Activities.CheckListSavedDataActivity;
import accsarathi.orimark.com.acclss.Activities.HomeslidingActivity;
import accsarathi.orimark.com.acclss.Adapters.DecisionPendingListAdapter;
import accsarathi.orimark.com.acclss.Model.DecisionPending;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.CustomItemClickListener;
import accsarathi.orimark.com.acclss.Util.MyUrl;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;
import accsarathi.orimark.com.acclss.Util.ServiceHandler;

import static android.content.ContentValues.TAG;


public class DecisionPendingFragment extends Fragment {

    View root;
    RecyclerView vehicle_status_list;
    String all_titles[] ={"Safety Audits","Decision Pending","Rejected","Allowed","Vehicle Movement","Repair Status"};
    int all_images[] = {R.drawable.status_audit,R.drawable.decision_pending,R.drawable.rejected,R.drawable.allowed,R.drawable.vehicle_movement,R.drawable.repair_status};
    public static DecisionPendingFragment newInstance() {
        return new DecisionPendingFragment();
    }
    ImageView header_image;
    TextView header_title;
    RelativeLayout imageView_containor;
    FragmentManager fragmentManager;
    ArrayList<DecisionPending> decisionPendingArrayList;
    NetworkConnection nw;
    ProgressDialog prgDialog;
    Boolean netConnection = false;
    String lssroleid;
    TextView txtNorcd;
    EditText search_edt;
    RelativeLayout search_lay;
    DecisionPendingListAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = null;
        root = inflater.inflate(R.layout.decision_pending_page, container, false);
        nw = new NetworkConnection(getActivity());
        prgDialog = new ProgressDialog(getActivity());
        prgDialog.setCancelable(false);

        search_lay = (RelativeLayout) root.findViewById(R.id.search_layout);
        search_lay.setVisibility(View.VISIBLE);
        search_edt = (EditText) root.findViewById(R.id.edit_search);
        vehicle_status_list = (RecyclerView) root.findViewById(R.id.vehicle_status_list);
        txtNorcd = root.findViewById(R.id.txtNorcd);
        txtNorcd.setVisibility(View.GONE);
        vehicle_status_list.setVisibility(View.GONE);
        header_title = (TextView)root.findViewById(R.id.title);
        imageView_containor = (RelativeLayout) root.findViewById(R.id.imageView_containor);
        header_title.setText("Decision Pending");
        header_image =(ImageView)root.findViewById(R.id.title_img);
        imageView_containor.setVisibility(View.VISIBLE);
        header_image.setImageResource(R.drawable.decision_pending);
        lssroleid = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.lssroleid);
        MyUrl.saveNotoDataWithKeyAndValue(getActivity(), "decision_noto_count", "0");

        search_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //if(charSequence.length()!=0){
                adapter.getFilter().filter(charSequence);
                // }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        HomeslidingActivity.menu_icon.setBackgroundResource(R.drawable.left_arrow_50x50);
        HomeslidingActivity.menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),HomeslidingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });


        new getAllDecisionPendingList().execute();

        return root;
    }

    public void selectItem(int position) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (position) {
            case 1:
                fragmentClass = DecisionPendingDetailFragment.class;
                break;

            default:
                fragmentClass = LandingFragment.class;
        }
        if (fragmentClass != null) {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }
    private class getAllDecisionPendingList extends AsyncTask<String, Void, Void> {

        String status, message;
        JSONObject json2;
        String response;
        String id ,name;
        ;
        @Override
        protected void onPreExecute() {
            prgDialog.setMessage(getResources().getString(R.string.otp_verifiy));
            prgDialog.show();
        }

        @Override
        protected Void doInBackground(String... urls) {

            if (nw.isConnectingToInternet() == true) {
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    ServiceHandler sh = new ServiceHandler();
                    nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.action, "acclssdecisionpendinglist"));
                    nameValuePairs.add(new BasicNameValuePair("lssroleid", lssroleid));

                    response = sh.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST,
                            nameValuePairs);
                    Log.e("response", response);
                    if(response.length()!=0) {
                        JSONObject js = new JSONObject(response);
                        status = js.getString(AllStaticVariables.status);


                        if (status.equalsIgnoreCase("1")) {
                            decisionPendingArrayList = new ArrayList<>();
                            JSONArray jsonArray = js.getJSONArray(AllStaticVariables.data);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject all_details = jsonArray.getJSONObject(i);
                                DecisionPending decisionPending = new DecisionPending();

                                decisionPending.setId(all_details.getString("escalateid"));
                                decisionPending.setCheck_list_id(all_details.getString("checklist_id"));
                                decisionPending.setLss_roll(all_details.getString("lssrole"));
                                decisionPending.setVehicle_reg_no(all_details.getString("vehicleregdno"));
                                decisionPending.setLocation(all_details.getString("location"));
                                decisionPending.setAud_by(all_details.getString("auditedby"));
                                decisionPending.setDate_time(all_details.getString("datetime"));
                                decisionPending.setStatus(all_details.getString("status"));
                                decisionPending.setFormtype(all_details.getString("formtype"));
                                decisionPending.setShowlist(all_details.getString("showlist"));

                                decisionPendingArrayList.add(decisionPending);
                            /* id = all_location.getString("id");
                            name = all_location.getString("name");
                            all_loc.add(name);
                            all_locations.put(name,id);*/
                            }
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                netConnection = true;
            } else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();

            if (netConnection == false) {
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                if (status.equalsIgnoreCase("1")) {
                    txtNorcd.setVisibility(View.GONE);
                    vehicle_status_list.setVisibility(View.VISIBLE);
                     adapter = new DecisionPendingListAdapter(getActivity(), decisionPendingArrayList, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position) {
                            Log.d(TAG, "clicked position:" + position);
                            // String quiz_title = (new ArrayList<>(all_quizzes.keySet())).get(position);
                            String checklist_id = decisionPendingArrayList.get(position).getCheck_list_id();
                            String vehicleregdno =decisionPendingArrayList.get(position).getVehicle_reg_no();
                            MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.vehicleregdno, vehicleregdno);
                            MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.checklist_id, checklist_id);
                            MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.escalated_id, decisionPendingArrayList.get(position).getId());

                            if("1".equalsIgnoreCase(decisionPendingArrayList.get(position).getShowlist())){
                                selectItem(1);
                            }else{
                                Intent checklistsavData= new Intent(getActivity(), CheckListSavedDataActivity.class);
                                checklistsavData.putExtra("checklistId",checklist_id);
                                getActivity().startActivity(checklistsavData);
                            }

                        }
                    });
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    vehicle_status_list.setLayoutManager(mLayoutManager);
                    vehicle_status_list.setItemAnimator(new DefaultItemAnimator());
                    vehicle_status_list.setAdapter(adapter);

                }else{
                    txtNorcd.setVisibility(View.VISIBLE);
                    vehicle_status_list.setVisibility(View.GONE);
                    search_lay.setVisibility(View.INVISIBLE);
                }
            }
            super.onPostExecute(result);
        }

    }



}
