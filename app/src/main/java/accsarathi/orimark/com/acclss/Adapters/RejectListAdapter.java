package accsarathi.orimark.com.acclss.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import accsarathi.orimark.com.acclss.Activities.CheckListSavedDataActivity;
import accsarathi.orimark.com.acclss.Model.RejectedModel;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.CustomItemClickListener;
import accsarathi.orimark.com.acclss.Util.MyUrl;

/**
 * Created by star on 3/1/2018.
 */

public class RejectListAdapter extends RecyclerView.Adapter<RejectListAdapter.MyViewHolder> implements Filterable {

    private Context mContext;
    CustomItemClickListener listener;
    View mRootview;
    List<RejectedModel> all_items;
    List<RejectedModel> all_items_filter;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtinspectionStatus,txtVechicleRegdNo, txtLocation, txtAuditedBy, txtLssRole, txtDate, txtStatus,vechicleType;

        public MyViewHolder(View view) {
            super(view);
            mRootview = view;
            txtVechicleRegdNo = view.findViewById(R.id.txtVechicleRegdNo);
            txtLocation = view.findViewById(R.id.txtLocation);
            txtAuditedBy = view.findViewById(R.id.txtAuditedBy);
            txtLssRole = view.findViewById(R.id.txtLssRole);
            txtDate = view.findViewById(R.id.txtDate);
            txtStatus = view.findViewById(R.id.txtStatus);
            vechicleType = view.findViewById(R.id.vechicleType);
            txtinspectionStatus = view.findViewById(R.id.txtinspectionStatus);
        }
    }


    public RejectListAdapter(Context mContext, ArrayList<RejectedModel> all_items) {
        this.mContext = mContext;
        this.all_items = all_items;
        this.all_items_filter = all_items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rejected_list_items, parent, false);

        final MyViewHolder mViewHolder = new MyViewHolder(itemView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
       try {
           final RejectedModel dataModel = all_items_filter.get(position);
           holder.txtVechicleRegdNo.setText("Vechicle Regd No: " + dataModel.getVehicleregdno());
           holder.txtLocation.setText(dataModel.getLocation());
           holder.txtAuditedBy.setText(dataModel.getAuditedby());
           holder.txtLssRole.setText(dataModel.getLssrole());
           holder.txtDate.setText(dataModel.getDatetime());
           holder.txtStatus.setText(dataModel.getStatus());
           holder.vechicleType.setText(dataModel.getFormtype());
           holder.txtinspectionStatus.setText(dataModel.getInspectionstatus());
           mRootview.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   MyUrl.saveDataWithKeyAndValue(mContext, AllStaticVariables.mySharedpreference.drvrpassportno, dataModel.getDrvrpassportno());
                   MyUrl.saveDataWithKeyAndValue(mContext, AllStaticVariables.mySharedpreference.vpassportno, dataModel.getVpassportno());

                   Intent checklistsavData = new Intent(mContext, CheckListSavedDataActivity.class);
                   checklistsavData.putExtra("checklistId", dataModel.getChecklist_id());
                   mContext.startActivity(checklistsavData);
               }
           });
       }catch (Exception e){
           e.printStackTrace();
       }

    }

    @Override
    public int getItemCount() {
        return all_items_filter.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    all_items_filter = all_items;
                } else {
                    List<RejectedModel> filteredList = new ArrayList<>();
                    for (RejectedModel row : all_items) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getVehicleregdno().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    all_items_filter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = all_items_filter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                all_items_filter = (ArrayList<RejectedModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
