package accsarathi.orimark.com.acclss.smsbroadcast;

/**
 * Created by prakash on 12/5/17.
 */

public interface SmsListener {
    public void messageReceived(String messageText);
}
