package accsarathi.orimark.com.acclss.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import accsarathi.orimark.com.acclss.Adapters.VechicleMovementAdapter;
import accsarathi.orimark.com.acclss.Model.VechicleMvmntModel;
import accsarathi.orimark.com.acclss.R;

/**
 * Created by prakash on 2/3/18.
 */

public class VechcicleMovementDetails extends AppCompatActivity {

    private Toolbar toolbar;
    private String vregistrationno_, drivername_, checklist_id_, currentstatus_, currentstatusid_;
    private ImageView header_image;
    private TextView header_title;
    private RelativeLayout imageView_containor;
    private EditText etVechRegNo, etDrvName, etCrntStatus;
    private RecyclerView vechiclevmntRecyclerview;
    ArrayList<VechicleMvmntModel> listOfVechicleMvmnt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vechicle_movement_details_apge);

        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);
        ImageButton imbBack = toolbar.findViewById(R.id.imbBack);
        imbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        String jsonArray = intent.getStringExtra("jsonArray");
        vregistrationno_ = intent.getStringExtra("vregistrationno");
        drivername_ = intent.getStringExtra("drivername");
        checklist_id_ = intent.getStringExtra("checklist_id");
        currentstatus_ = intent.getStringExtra("currentstatus");
        currentstatusid_ = intent.getStringExtra("currentstatusid");

        try {
            JSONArray array = new JSONArray(jsonArray);
            System.out.println(array.toString(2));
            listOfVechicleMvmnt = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                if(i<=2){
                    JSONObject obj_ = array.getJSONObject(i);
                    VechicleMvmntModel vechicleMvmntModel = new VechicleMvmntModel(obj_.getString("id"),
                            obj_.getString("checklist_id"),
                            obj_.getString("vehicleregdno"),
                            obj_.getString("location"),
                            obj_.getString("auditedby"),
                            obj_.getString("lssrole"),
                            obj_.getString("fcomment"),
                            obj_.getString("datetime"),
                            obj_.getString("formtype"),
                            obj_.getString("auditedresult"),
                            obj_.getString("auditedresultstatus"),
                            obj_.getString("vehiclestatus"),
                            obj_.getString("incValue"));
                             //obj_.getString("vehiclestatus"));

                    listOfVechicleMvmnt.add(vechicleMvmntModel);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        initViewIds();

    }

    private void initViewIds() {
        header_title = findViewById(R.id.title);
        header_title.setText("Vehicle Movement Details");
        header_image = findViewById(R.id.title_img);
        imageView_containor = findViewById(R.id.imageView_containor);
        imageView_containor.setVisibility(View.VISIBLE);
        header_image.setImageResource(R.drawable.vehicle_movement);

        etVechRegNo = findViewById(R.id.etVechRegNo);
        etDrvName = findViewById(R.id.etDrvName);
        etCrntStatus = findViewById(R.id.etCrntStatus);
        vechiclevmntRecyclerview = findViewById(R.id.vechiclevmntRecyclerview);

        setVechicleInfoAtHeader();
       /* Collections.sort(listOfVechicleMvmnt, new Comparator<VechicleMvmntModel>() {
            public int compare(VechicleMvmntModel obj1, VechicleMvmntModel obj2) {
                return obj2.getIncValue().compareToIgnoreCase(obj1.getIncValue());
            }
        });*/
        VechicleMovementAdapter adapter = new VechicleMovementAdapter(VechcicleMovementDetails.this, listOfVechicleMvmnt);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(VechcicleMovementDetails.this);
        vechiclevmntRecyclerview.setLayoutManager(mLayoutManager);
        vechiclevmntRecyclerview.setItemAnimator(new DefaultItemAnimator());
        vechiclevmntRecyclerview.setAdapter(adapter);

    }

    private void setVechicleInfoAtHeader() {
        etVechRegNo.setText(vregistrationno_);
        etDrvName.setText(drivername_);
        etCrntStatus.setText(currentstatus_);
    }

}
