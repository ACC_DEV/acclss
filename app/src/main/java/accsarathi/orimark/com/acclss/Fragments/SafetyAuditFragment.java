package accsarathi.orimark.com.acclss.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.reginald.editspinner.EditSpinner;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import accsarathi.orimark.com.acclss.Activities.HomeslidingActivity;
import accsarathi.orimark.com.acclss.Activities.LoginActivity;
import accsarathi.orimark.com.acclss.Activities.OTPActivity;
import accsarathi.orimark.com.acclss.Adapters.SafetyAuditAdapter;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.ApiHelper;
import accsarathi.orimark.com.acclss.Util.CustomItemClickListener;
import accsarathi.orimark.com.acclss.Util.MyUrl;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;
import accsarathi.orimark.com.acclss.Util.ServiceHandler;

import static android.content.ContentValues.TAG;


public class SafetyAuditFragment extends Fragment {

    View root;
    RecyclerView vehicle_status_list;
    String all_titles[] ={"Bulker Safety Inspection","Inbound Vehicle Inspection","Outbound Vehicle Inspection"};
    int all_images[] = {R.drawable.blucker_vehicle,R.drawable.inbound_vehicle,R.drawable.outbound_vehicle};

    ImageView header_image;
    TextView header_title;
    RelativeLayout imageView_containor;
    FragmentManager fragmentManager;
    String lss_roll,name;
    EditText name_edt,roll_edt;
    AppCompatSpinner mEditSpinner;
    NetworkConnection nw;
    ProgressDialog prgDialog;
    Boolean netConnection = false;
    HashMap<String,String> all_locations = new HashMap<>();
    ArrayList<String> all_loc = new ArrayList<>();

    public static SafetyAuditFragment newInstance() {
        return new SafetyAuditFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = null;
        root = inflater.inflate(R.layout.safety_audit_page, container, false);
        vehicle_status_list = (RecyclerView) root.findViewById(R.id.vehicle_status_list);
        header_title = (TextView)root.findViewById(R.id.title);
        header_title.setText("Vehicle Safety Audit");
        header_image =(ImageView)root.findViewById(R.id.title_img);
        imageView_containor = (RelativeLayout) root.findViewById(R.id.imageView_containor);
        imageView_containor.setVisibility(View.VISIBLE);
        header_image.setImageResource(R.drawable.status_audit);
        name_edt = root.findViewById(R.id.name_edt);
        roll_edt = root.findViewById(R.id.lcc_role_edt);
        name = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.username);
        lss_roll = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.lssrole);
        roll_edt.setText(lss_roll);
        name_edt.setText(name);

        mEditSpinner = (AppCompatSpinner) root.findViewById(R.id.edit_spinner);
        /*ListAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.your_string_array));
        mEditSpinner.setAdapter(adapter);*/

        mEditSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String location_name = all_loc.get(position).toString();
                String location_id = all_locations.get(location_name).toString();

                MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.selected_location, location_name);
                MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.selected_location_id, location_id);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        SafetyAuditAdapter adapter = new SafetyAuditAdapter(getActivity(), all_titles,all_images, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Log.d(TAG, "clicked position:" + position);



             //   int postion = mEditSpinner.getListSelection();
                 selectItem(position);

            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        vehicle_status_list.setLayoutManager(mLayoutManager);
        vehicle_status_list.setItemAnimator(new DefaultItemAnimator());
        vehicle_status_list.setAdapter(adapter);
        HomeslidingActivity.menu_icon.setBackgroundResource(R.drawable.left_arrow_50x50);
        HomeslidingActivity.menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),HomeslidingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });
        nw = new NetworkConnection(getActivity());
        prgDialog = new ProgressDialog(getActivity());
        prgDialog.setCancelable(false);
        if (nw.isConnectingToInternet() == true) {
            new downloadTaskOperation().execute();
        } else {

            Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        }



        return root;
    }
    public void selectItem(int position) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (position) {
            case 0:
                fragmentClass = BulkerSafetyCheckFragment.class;
                break;
            case 1:
                fragmentClass = InboundSafetyCheckFragment.class;
                break;
            case 2:
                fragmentClass = OutboundSafetyCheckFragment.class;
                break;
            default:
                 fragmentClass = LandingFragment.class;
        }
        if (fragmentClass != null) {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }




    private class downloadTaskOperation extends AsyncTask<String, Void, Void> {

        String status, message;
        JSONObject json2;
        String response;
        String id ,name;
        @Override
        protected void onPreExecute() {
            prgDialog.setMessage(getResources().getString(R.string.otp_verifiy));
            prgDialog.show();
        }

        @Override
        protected Void doInBackground(String... urls) {

            if (nw.isConnectingToInternet() == true) {
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    ServiceHandler sh = new ServiceHandler();
                    nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.action, "acclssalllocation"));
                     response = sh.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST,
                            nameValuePairs);
                    Log.e("response", response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString(AllStaticVariables.status);
                    JSONArray jsonArray = js.getJSONArray(AllStaticVariables.data);

                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject all_location = jsonArray.getJSONObject(i);
                         id = all_location.getString("id");
                         name = all_location.getString("name");
                        all_loc.add(name);
                        all_locations.put(name,id);
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                netConnection = true;
            } else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();

            if (netConnection == false) {
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                if (status.equalsIgnoreCase("1")) {
                    String names[]=all_loc.toArray(new String[all_loc.size()]);
                    ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,names);
                    mEditSpinner.setAdapter(adapter);
                    mEditSpinner.setSelection(0);
                    String location_name = all_loc.get(0).toString();
                    String location_id = all_locations.get(location_name).toString();
                    MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.selected_location, location_name);
                    MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.selected_location_id, location_id);

                }
            }
            super.onPostExecute(result);
        }

    }

}
