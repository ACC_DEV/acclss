package accsarathi.orimark.com.acclss.Util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by prakash on 21/09/16.
 */
public class MyUrl {

    public static String baseAPI = "http://www.koolfeedback.com/acclss/API/v1/accapiv1.php";

    //public static String signupAPI = "http://koolfeedback.com/acc/API/v1/normaluser.php?action=signup";

   // public static String signupAPI=baseAPI+"action=signup";
    //public static String loginAPI=baseAPI+"action=signin";
    public static void saveDataWithKeyAndValue(Context context, String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("Kf_user", context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getDataFromKey(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences("Kf_user", context.MODE_PRIVATE);
        String restoredText = prefs.getString(key, null);
        return restoredText;

    }


    public static void saveNotoDataWithKeyAndValue(Context context, String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("Kf_user", context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getNotoDataFromKey(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences("Kf_user", context.MODE_PRIVATE);
        String restoredText = prefs.getString(key, "0");
        return restoredText;

    }

}
