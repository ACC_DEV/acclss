package accsarathi.orimark.com.acclss.Fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import accsarathi.orimark.com.acclss.Activities.HomeslidingActivity;
import accsarathi.orimark.com.acclss.Adapters.RejectListAdapter;
import accsarathi.orimark.com.acclss.Model.RejectedModel;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.ApiHelper;
import accsarathi.orimark.com.acclss.Util.MyUrl;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;


public class RejectedFragment extends Fragment {

    View root;
    RecyclerView vehicle_status_list;
    public static RejectedFragment newInstance() {
        return new RejectedFragment();
    }

    ImageView header_image;
    TextView header_title;
    FragmentManager fragmentManager;
    RelativeLayout imageView_containor;
    NetworkConnection nw;
    ArrayList<RejectedModel> rejectArraylist;
    TextView txtNoData;
    EditText search_edt;
    RelativeLayout search_lay;
    RejectListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = null;
        root = inflater.inflate(R.layout.rejected_page, container, false);
        vehicle_status_list = (RecyclerView) root.findViewById(R.id.vehicle_status_list);
        vehicle_status_list.setVisibility(View.GONE);
        txtNoData = root.findViewById(R.id.txtNoData);
        txtNoData.setVisibility(View.GONE);
        search_lay = (RelativeLayout) root.findViewById(R.id.search_layout);
        search_lay.setVisibility(View.VISIBLE);
        search_edt = (EditText) root.findViewById(R.id.edit_search);
        header_title = (TextView) root.findViewById(R.id.title);
        header_title.setText("Rejected Vehicle List");
        header_image = (ImageView) root.findViewById(R.id.title_img);
        imageView_containor = (RelativeLayout) root.findViewById(R.id.imageView_containor);
        imageView_containor.setVisibility(View.VISIBLE);
        header_image.setImageResource(R.drawable.rejected);
        MyUrl.saveNotoDataWithKeyAndValue(getActivity(), "reject_noto_count", "0");

        nw = new NetworkConnection(getActivity());
        if (nw.isConnectingToInternet()) {
            new ApiHelper(getActivity(), AllStaticVariables.apiAction.reject_list, rejectListener).execute();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        }
        search_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //if(charSequence.length()!=0){
                adapter.getFilter().filter(charSequence);
                // }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        HomeslidingActivity.menu_icon.setBackgroundResource(R.drawable.left_arrow_50x50);
        HomeslidingActivity.menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HomeslidingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });
        return root;
    }

    private ApiHelper.TaskDelegate rejectListener = new ApiHelper.TaskDelegate() {
        public void onTaskFisnishGettingData(Object result) {
            try {
                if (result != null) {
                    JSONObject json = (JSONObject) result;
                    rejectArraylist = new ArrayList<>();
                    int dataStatus_= json.getInt("status");
                    if(dataStatus_==1){
                        vehicle_status_list.setVisibility(View.VISIBLE);
                        txtNoData.setVisibility(View.GONE);
                        final JSONArray rejectedArr = json.getJSONArray("data");
                        for (int i = 0; i < rejectedArr.length(); i++) {
                            JSONObject jsonObject = rejectedArr.getJSONObject(i);
                            String id=jsonObject.getString("id");
                            String checklist_id=jsonObject.getString("checklist_id");
                            String vehicleregdno=jsonObject.getString("vehicleregdno");
                            String location=jsonObject.getString("location");
                            String auditedby=jsonObject.getString("auditedby");
                            String lssrole=jsonObject.getString("lssrole");
                            String datetime=jsonObject.getString("datetime");
                            String status=jsonObject.getString("status");
                            String formTYpe=jsonObject.getString("formtype");
                            String vpassportno=jsonObject.getString("vpassportno");
                            String drvrpassportno=jsonObject.getString("drvrpassportno");
                            String inspectionstatus=jsonObject.getString("inspectionstatus");

                            RejectedModel rejectedModel = new RejectedModel(id, checklist_id, vehicleregdno, location, auditedby, lssrole, datetime, status,formTYpe,vpassportno,drvrpassportno,inspectionstatus);
                            rejectArraylist.add(rejectedModel);
                            adapter = new RejectListAdapter(getActivity(), rejectArraylist);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            vehicle_status_list.setLayoutManager(mLayoutManager);
                            vehicle_status_list.setItemAnimator(new DefaultItemAnimator());
                            vehicle_status_list.setAdapter(adapter);
                        }
                    }else{
                        vehicle_status_list.setVisibility(View.GONE);
                        txtNoData.setVisibility(View.VISIBLE);
                        search_lay.setVisibility(View.INVISIBLE);
                    }

                }
            } catch (Exception e) {
                Log.e("Error", e.toString());
            }
        }
    };

    public void selectItem(int position) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (position) {


            default:
                fragmentClass = LandingFragment.class;
        }
        if (fragmentClass != null) {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
