package accsarathi.orimark.com.acclss.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import accsarathi.orimark.com.acclss.Fragments.SafetyAuditFragment;
import accsarathi.orimark.com.acclss.Fragments.VehiclePermissionFragment;
import accsarathi.orimark.com.acclss.Model.QuestionAnsModel;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.ApiHelper;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;


public class CheckListSavedDataActivity extends AppCompatActivity {

    ImageView header_image;
    TextView header_title;
    RelativeLayout imageView_containor;
    FragmentManager fragmentManager;
    ListView listView;
    BulkerCheckListAdapter bulkerCheckListAdapter;
    ArrayList<String> bulker_list;
    EditText v_pass_edt,d_pass_edt,final_comment, v_regd_edt, driver_name_edt, oper_edt, location_edt;
    LinearLayout approve_lay, con_approve_lay, reject_lay;
    RelativeLayout footer_button_lay;
    Button ok_btn, allow_btn, escalate_btn;
    NetworkConnection nw;
    ProgressDialog prgDialog;
    Boolean netConnection = false;
    String checklist_id, lssrole, lssroleid, plantid, alllowesclatestatus, user_id, location_name, location_id, vehicleid, driverid, vehicleregdno, payloadcapacity, drivername, driverdlno, drivermobileno, transportername, transporterid, transportcontractorsupervisor;
    LinearLayout.LayoutParams param_1, param_2;
    String drvrpassportno,vpassportno,operator, plantlocation, fcomment, datetime, chkliststatus, formtype;
    //added by pb
    private EditText etLssRole, etAuditedby, etDate;
    private String etLssRole_, etAuditedby_, etDate_,formtypeid;
    TextView vechicleType;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.saved_form_main_layout);

        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);
        ImageButton imbBack = toolbar.findViewById(R.id.imbBack);
        imbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Bundle b = getIntent().getExtras();
        if (b != null) {
            checklist_id = b.getString("checklistId");
            Log.e("checklistId==", checklist_id);
        }
        nw = new NetworkConnection(CheckListSavedDataActivity.this);
        prgDialog = new ProgressDialog(CheckListSavedDataActivity.this);
        prgDialog.setCancelable(false);


        listView = (ListView) findViewById(R.id.list);


        View headerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.header_save_form_lay, null, false);
        header_title = (TextView) headerView.findViewById(R.id.title);
        imageView_containor = (RelativeLayout) headerView.findViewById(R.id.imageView_containor);
        header_title.setText("Checklist Saved Data");
        header_image = (ImageView) headerView.findViewById(R.id.title_img);
        imageView_containor.setVisibility(View.VISIBLE);
        v_regd_edt = (EditText) headerView.findViewById(R.id.v_regd_edt);
        driver_name_edt = (EditText) headerView.findViewById(R.id.driver_name_edt);
        oper_edt = (EditText) headerView.findViewById(R.id.oper_edt);
        location_edt = (EditText) headerView.findViewById(R.id.location_edt);
        vechicleType = headerView.findViewById(R.id.vechicleType);
        etAuditedby = headerView.findViewById(R.id.etAuditedby);
        etLssRole = headerView.findViewById(R.id.etLssRole);
        etDate = headerView.findViewById(R.id.etDate);
        d_pass_edt = headerView.findViewById(R.id.d_pass_edt);
        v_pass_edt = headerView.findViewById(R.id.v_pass_edt);

        param_1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1.0f
        );
        param_1.setMargins(10, 0, 10, 0);
        param_2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                2.0f
        );
        param_2.setMargins(10, 0, 10, 0);
        //YOUR_VIEW.setLayoutParams(param);


        View footerView = ((LayoutInflater) CheckListSavedDataActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.checklist_data_view_footer_lay, null, false);
        final_comment = (EditText) footerView.findViewById(R.id.final_comment);
        footer_button_lay = (RelativeLayout) footerView.findViewById(R.id.footer_button_lay);
        approve_lay = (LinearLayout) footerView.findViewById(R.id.approve_lay);
        con_approve_lay = (LinearLayout) footerView.findViewById(R.id.con_approve_lay);
        reject_lay = (LinearLayout) footerView.findViewById(R.id.reject_lay);
        ok_btn = (Button) footerView.findViewById(R.id.ok_btn);


        listView.addHeaderView(headerView);
        listView.addFooterView(footerView);

        driver_name_edt.setText(drivername);
        v_regd_edt.setText(vehicleregdno);
        location_edt.setText(location_name);
        oper_edt.setText(transportername);

        /*HomeslidingActivity.menu_icon.setBackgroundResource(R.drawable.left_arrow_50x50);
        HomeslidingActivity.menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectItem(10);
            }
        });
*/
        if (nw.isConnectingToInternet()) {
            new ApiHelper(CheckListSavedDataActivity.this, AllStaticVariables.apiAction.getSaveCheckList, checklist_id, saveCheckListDataListener).execute();
        } else {

            Toast.makeText(CheckListSavedDataActivity.this, getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        }
    }

    private ApiHelper.TaskDelegate saveCheckListDataListener = new ApiHelper.TaskDelegate() {
        public void onTaskFisnishGettingData(Object result) {
            try {
                if (result != null) {
                    JSONObject json = (JSONObject) result;
                    System.out.println("save chk list data === " + json);
                    ArrayList<QuestionAnsModel> bulker_list_qes_ans = new ArrayList<>();
                    final JSONArray rejectedArr = json.getJSONArray("data");
                    JSONObject jsonObject = rejectedArr.getJSONObject(0);
                    String id = jsonObject.getString("id");
                    drivername = jsonObject.getString("drivername");
                    vehicleregdno = jsonObject.getString("vehicleregdno");
                    operator = jsonObject.getString("operator");
                    plantlocation = jsonObject.getString("plantlocation");
                    fcomment = jsonObject.getString("fcomment");
                    datetime = jsonObject.getString("datetime");
                    chkliststatus = jsonObject.getString("chkliststatus");
                    formtype = jsonObject.getString("formtype");
                    etLssRole_ = jsonObject.getString("lssrole");
                    etAuditedby_ = jsonObject.getString("auditedby");
                    etDate_ = jsonObject.getString("datetime");
                    formtypeid = jsonObject.getString("formtypeid");

                    vpassportno = jsonObject.getString("vpassportno");
                    drvrpassportno = jsonObject.getString("drvrpassportno");

                    JSONArray questionAnsArray = jsonObject.getJSONArray("question");
                    Log.e("form type= ", formtype);
                    if ("1".equalsIgnoreCase(formtypeid)) {
                        bulkerData();
                    } else if ("2".equalsIgnoreCase(formtypeid)) {
                        inBoundData();
                    } else {
                        outBoundData();//Outbound
                    }

                    for (int q = 0; q < questionAnsArray.length(); q++) {
                        JSONObject obj_ = questionAnsArray.getJSONObject(q);
                        String queID = obj_.getString("queID");
                        String answer = obj_.getString("answer");
                        String comments = obj_.getString("comments");
                        QuestionAnsModel questionAnsModel = new QuestionAnsModel(queID, answer, comments);
                        bulker_list_qes_ans.add(questionAnsModel);
                    }

                    setHeaderViewData();
                    bulkerCheckListAdapter = new BulkerCheckListAdapter(CheckListSavedDataActivity.this, bulker_list, bulker_list_qes_ans, formtype);
                    listView.setAdapter(bulkerCheckListAdapter);

                }
            } catch (Exception e) {
                Log.e("Error", e.toString());
            }
        }
    };

    private void setHeaderViewData() {
        v_regd_edt.setText(vehicleregdno);
        driver_name_edt.setText(drivername);
        oper_edt.setText(operator);
        location_edt.setText(plantlocation);
        vechicleType.setText(formtype);
        etAuditedby.setText(etAuditedby_);
        etLssRole.setText(etLssRole_);
        etDate.setText(etDate_);
        v_pass_edt.setText(vpassportno);
        d_pass_edt.setText(drvrpassportno);
        if ("Bulker".equalsIgnoreCase(formtype)) {
            header_image.setImageResource(R.drawable.blucker_vehicle);
        } else if ("Inbound".equalsIgnoreCase(formtype)) {
            header_image.setImageResource(R.drawable.inbound_vehicle);
        } else {
            header_image.setImageResource(R.drawable.outbound_vehicle);
        }


        final_comment.setText(fcomment);
        final_comment.setEnabled(false);
        approve_lay.setVisibility(View.GONE);
        ok_btn.setText("BACK");
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void bulkerData() {
        bulker_list = new ArrayList<>();
        bulker_list.add("DRIVER");
        bulker_list.add("Driver has valid Driving Licence and renewed (as applicable)");
        bulker_list.add("Driver passed Alcohol Test (Blood Alcohol level)");
        bulker_list.add("Driver has PPE (Safety Helmet, Safety Shoes and Reflective Jacket)");
        bulker_list.add("VEHICLE");
        bulker_list.add("Valid Bulker testing certificate as per \"Factories Act\" is available (Check for expiry date)");
        bulker_list.add("RC Book / Fitness Certificate/ Insurance/PUC documents available and renewed (as applicable)");
        bulker_list.add("Pressure gauge is fixed on air line");
        bulker_list.add("Safety valve is fixed on air line");
        bulker_list.add("Head Lights & Rear lights in working order");
        bulker_list.add("Brakes in working order");
        bulker_list.add("Hand brake in working order");
        bulker_list.add("Windscreen wipers in working order");
        bulker_list.add("All tyres (including spares) in roadworthy condition (treads not worn out)");
        bulker_list.add("No hazardous goods kept inside the Cabin (Stove / Kerosene etc.)");
        bulker_list.add("Outlet hose is free from any wear & tear and attached with four bolts to bulker tanker");
        bulker_list.add("All coupling latches are available on air line and material line");
        bulker_list.add("Butterfly valve on air line and material line is available");
    }

    public void inBoundData() {
        bulker_list = new ArrayList<>();

        bulker_list.add("DRIVER");

        bulker_list.add("Driver Valid License Available");
        bulker_list.add("Driver Passed Alcohol Test");
        bulker_list.add("Driver to have PPE (Safety Shoes, Reflective Vest & Helmet)");

        bulker_list.add("VEHICLE");

        bulker_list.add("Availability of Rear View Mirrors on both sides");
        bulker_list.add("Horn in working order");
        bulker_list.add("Reverse Alarm & Lights in working order");
        bulker_list.add("Proper Windscreen without any cracks");
        bulker_list.add("Head Lights & Rear lights in working order");
        bulker_list.add("Brakes in working order");
        bulker_list.add("Hand brake in working order");
        bulker_list.add("Windscreen wipers in working order");
        bulker_list.add("Availability of wheel chocks");
        bulker_list.add("Tyres (including spare) in roadworthy condition ");
        bulker_list.add("Parking Lights in working order");
        bulker_list.add("Side Indicators in working order");
        bulker_list.add("Emergency Contact Numbers Available");
        bulker_list.add("Wheel Nut-Bolts properly secured");
        bulker_list.add("First Aid Box available in vehicle");
        bulker_list.add("Fire Extinguisher available in vehicle (in valid condition)");
        bulker_list.add("RC Book / PUC / Fitness Certificate documents available");
        bulker_list.add("Any hazardous goods kept inside the Cabin (Stove / Kerosene etc.)");
        bulker_list.add("Any visible Oil Leakage");
        bulker_list.add("Availability of Traffic Cones and Safety Triangle for Breakdown Vehicle");
        bulker_list.add("Safety Triangle Fitted in Front & Rear of the Vehicle");
        bulker_list.add("Availability of Emergency Light");
        bulker_list.add("Flashing Amber Light for night time in-plant operations");
        bulker_list.add("Tool Box (Jack/Handle) available in vehicle");
        bulker_list.add("Bucket Seat for Driver");
        bulker_list.add("Retractable  3-point Seat Belt for Driver");
        bulker_list.add("Bucket Seat for Cleaner");
        bulker_list.add("Retractable 3-point  Seat Belt for co-driver/helper");
        bulker_list.add("Availability of Reflective Strip");
        bulker_list.add("Side & Rear Under-run Protection Device (SUPD/RUPD) fitted");

    }

    public void outBoundData() {
        bulker_list = new ArrayList<>();

        bulker_list.add("DRIVER");
        bulker_list.add("Driver has valid Driving Licence ");
        bulker_list.add("Driver passed Alcohol Test (Blood Alcohol level)");
        bulker_list.add("Driver has PPE (Safety Helmet, Safety Shoes and Reflective Jacket)");

        bulker_list.add("VEHICLE");
        bulker_list.add("RC Book / Fitness Certificate/ Insurance/Pollution documents available");
        bulker_list.add("Head Lights & Rear lights in working order");
        bulker_list.add("Brakes in working order");
        bulker_list.add("Hand brake in working order");
        bulker_list.add("Windscreen wipers in working order");
        bulker_list.add("All tyres (including spares) in roadworthy condition (treads not worn out)");
        bulker_list.add("No hazardous goods kept inside the Cabin (Stove / Kerosene etc.)");

    }


    public void selectItem(int position) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (position) {
            case 1:
                fragmentClass = VehiclePermissionFragment.class;
                break;
            default:
                fragmentClass = SafetyAuditFragment.class;
        }
        if (fragmentClass != null) {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager = CheckListSavedDataActivity.this.getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    public class BulkerCheckListAdapter extends BaseAdapter {

        private Activity activity;
        private LayoutInflater inflater = null;
        ArrayList<String> bulker_list;
        ArrayList<QuestionAnsModel> bulker_list_qes_ans;
        String formtype;

        public BulkerCheckListAdapter(Activity a, ArrayList<String> bulker_list, ArrayList<QuestionAnsModel> bulker_list_qes_ans, String formtype) {
            activity = a;
            this.bulker_list = bulker_list;
            this.bulker_list_qes_ans = bulker_list_qes_ans;
            this.formtype = formtype;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            return bulker_list.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            vi = null;

            //final String QSTANSTitleArr[] = bulker_list.get(position).toString().split(Pattern.quote("|"));
            if (position == 0) {
                vi = inflater.inflate(R.layout.bulker_list_inner_header_items, null);
                TextView header_title = (TextView) vi.findViewById(R.id.h_title); // title
                header_title.setText(bulker_list.get(position).toString());

            } else if (position == 4) {
                vi = inflater.inflate(R.layout.bulker_list_inner_header_items, null);
                TextView header_title = (TextView) vi.findViewById(R.id.h_title); // title
                header_title.setText(bulker_list.get(position).toString());

            } else {

                vi = inflater.inflate(R.layout.checklist_save_quest_ans_display, null);
                ImageView star_img = vi.findViewById(R.id.star_icon);
                TextView questions = vi.findViewById(R.id.questions); // title
                TextView txtAns = vi.findViewById(R.id.txtAns); // ans
                TextView txtCommnt = vi.findViewById(R.id.txtCommnt); // cmnt
                RelativeLayout mainlay = vi.findViewById(R.id.mainlay);
                //System.out.println("adadadada =" + QSTANSTitleArr[0] + "====" + QSTANSTitleArr[1]);
                questions.setText(bulker_list.get(position).toString());

                for (int i = 0; i < bulker_list_qes_ans.size(); i++) {
                    QuestionAnsModel model_ = bulker_list_qes_ans.get(i);
                    if (position == 1 && "1".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());

                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }


                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 2 && "2".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());

                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }

                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 3 && "3".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 5 && "4".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 6 && "5".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 7 && "6".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 8 && "7".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 9 && "8".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 10 && "9".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 11 && "10".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 12 && "11".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 13 && "12".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 14 && "13".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 15 && "14".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 16 && "15".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 17 && "16".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 18 && "17".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 19 && "18".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 20 && "19".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 21 && "20".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 22 && "21".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 23 && "22".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 24 && "23".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 25 && "24".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 26 && "25".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 27 && "26".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 28 && "27".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 29 && "28".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 30 && "29".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 31 && "30".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 32 && "31".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 33 && "32".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    } else if (position == 34 && "33".equalsIgnoreCase(model_.getQueID())) {
                        txtAns.setText("Ans: " + model_.getAnswer());
                        if(model_.getAnswer().equalsIgnoreCase("yes")){
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_green_border);
                            txtAns.setTextColor(getResources().getColor(R.color.green));
                        }else{
                            mainlay.setBackgroundResource(R.drawable.rounded_corner_red_border);
                            txtAns.setTextColor(getResources().getColor(R.color.red));
                            txtCommnt.setTextColor(getResources().getColor(R.color.red));
                        }
                        if (!model_.getComments().equalsIgnoreCase("")) {
                            txtCommnt.setVisibility(View.VISIBLE);
                            txtCommnt.setText("Comment: " + model_.getComments());
                        }
                    }

                }

                if (position >= 0 && position <= 11) {
                    star_img.setVisibility(View.VISIBLE);
                } else {
                    star_img.setVisibility(View.GONE);
                }

            }

            return vi;
        }
    }

}
