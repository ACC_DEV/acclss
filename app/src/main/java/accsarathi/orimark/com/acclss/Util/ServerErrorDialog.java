package accsarathi.orimark.com.acclss.Util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by prakash on 01/12/16.
 */
public class ServerErrorDialog {
    Context context;
    String description;

    public ServerErrorDialog(String Description, Context c) {
        this.description=Description;
        this.context = c;
    }

    private void ErrorDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("You get Error...");
        alertDialog.setMessage(description);

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }
}
