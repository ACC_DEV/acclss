package accsarathi.orimark.com.acclss.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import accsarathi.orimark.com.acclss.Activities.HomeslidingActivity;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.MyUrl;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;
import accsarathi.orimark.com.acclss.Util.ServiceHandler;


public class OutboundVehicleCheckListFragment extends Fragment {

    View root;
    ImageView header_image;
    TextView header_title;
    RelativeLayout imageView_containor;
    FragmentManager fragmentManager;
    ListView listView;
    BulkerCheckListAdapter bulkerCheckListAdapter;
    ArrayList<String> bulker_list,bulker_list_qes;
    EditText v_pass_edt,d_pass_edt,final_comment,v_regd_edt,driver_name_edt,oper_edt,location_edt;
    LinearLayout approve_lay,con_approve_lay,reject_lay,footer_button_lay;
    Button allow_btn,escalate_btn;
    NetworkConnection nw;
    ProgressDialog prgDialog;
    Boolean netConnection = false;
    String escbuttonshow,vpassportno,drvrpassportno,lssrole,lssroleid,plantid,alllowesclatestatus,user_id,location_name,location_id,vehicleid,driverid,vehicleregdno,payloadcapacity,drivername,driverdlno,drivermobileno,transportername,transporterid,transportcontractorsupervisor;
    public static ArrayList<String> selectedAnswers;
    TreeMap<Integer,String> selected_ques_ans = new TreeMap<>();
    HashMap<Integer,String> selected_ques_ans_comm = new HashMap<>();
    LinearLayout.LayoutParams param_1,param_2;
    public static OutboundVehicleCheckListFragment newInstance() {
        return new OutboundVehicleCheckListFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = null;
        root = inflater.inflate(R.layout.bulker_safety_check_list_page, container, false);
        nw = new NetworkConnection(getActivity());
        prgDialog = new ProgressDialog(getActivity());
        prgDialog.setCancelable(false);

        prepareData();


        listView =(ListView) root.findViewById(R.id.list);


        View headerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.header_lay, null, false);
        header_title = (TextView)headerView.findViewById(R.id.title);
        imageView_containor = (RelativeLayout) headerView.findViewById(R.id.imageView_containor);
        header_title.setText("Outbound Vehicle Checklist");
        header_image =(ImageView)headerView.findViewById(R.id.title_img);
        imageView_containor.setVisibility(View.VISIBLE);
        header_image.setImageResource(R.drawable.blucker_vehicle);
        v_regd_edt =(EditText) headerView.findViewById(R.id.v_regd_edt);
        driver_name_edt =(EditText) headerView.findViewById(R.id.driver_name_edt);
        oper_edt =(EditText) headerView.findViewById(R.id.oper_edt);
        location_edt =(EditText) headerView.findViewById(R.id.location_edt);
        d_pass_edt =(EditText) headerView.findViewById(R.id.d_pass_edt);
        v_pass_edt =(EditText) headerView.findViewById(R.id.v_pass_edt);






        param_1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1.0f
        );
        param_1.setMargins(10, 0, 10, 0);
        param_2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                2.0f
        );
        param_2.setMargins(10, 0, 10, 0);
        //YOUR_VIEW.setLayoutParams(param);



        View footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_lay, null, false);
        final_comment = (EditText)footerView.findViewById(R.id.final_comment);
        footer_button_lay = (LinearLayout)footerView.findViewById(R.id.footer_button_lay);
        approve_lay = (LinearLayout)footerView.findViewById(R.id.approve_lay);
        con_approve_lay = (LinearLayout)footerView.findViewById(R.id.con_approve_lay);
        reject_lay = (LinearLayout)footerView.findViewById(R.id.reject_lay);
        allow_btn = (Button) footerView.findViewById(R.id.allow_btn);
        escalate_btn = (Button) footerView.findViewById(R.id.escalate_btn);

       /* approve_lay.setEnabled(false);
        con_approve_lay.setEnabled(false);
        reject_lay.setEnabled(false);
        allow_btn.setEnabled(false);
        escalate_btn.setEnabled(false);
        escalate_btn.setClickable(false);
        escalate_btn.setAlpha(0.7f);*/

        listView.addHeaderView(headerView);
        listView.addFooterView(footerView);

        bulkerCheckListAdapter = new BulkerCheckListAdapter(getActivity(),bulker_list);

        listView.setAdapter(bulkerCheckListAdapter);


        vehicleid = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.vehicleid);
        driverid = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.driverid);
        vehicleregdno = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.vehicleregdno);
        payloadcapacity = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.payloadcapacity);
        drivername = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.drivername);
        driverdlno = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.driverdlno);
        drivermobileno = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.drivermobileno);
        transportername = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.transportername);
        transporterid = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.transporterid);
        transportcontractorsupervisor = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.transportcontractorsupervisor);
        location_name = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.selected_location);
        location_id = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.selected_location_id);
        user_id = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.userid);
        plantid = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.plantid);
        lssrole = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.lssrole);
        lssroleid = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.lssroleid);
        drvrpassportno = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.drvrpassportno);
        vpassportno = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.vpassportno);
        escbuttonshow = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.escbuttonshow);

        if(escbuttonshow.equalsIgnoreCase("0")){
            escalate_btn.setText("NOT ALLOW");
        }else{
            escalate_btn.setText("ESCALATE");
        }


        driver_name_edt.setText(drivername);
        v_regd_edt.setText(vehicleregdno);
        location_edt.setText(location_name);
        oper_edt.setText(transportername);
        v_pass_edt.setText(vpassportno);
        d_pass_edt.setText(drvrpassportno);

        HomeslidingActivity.menu_icon.setBackgroundResource(R.drawable.left_arrow_50x50);
        HomeslidingActivity.menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectItem(10);
            }
        });

        allow_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alllowesclatestatus= "1";

                if(selected_ques_ans.size()==10) {
                    if (nw.isConnectingToInternet()) {
                        new downloadReOtpOperation().execute();
                    } else {

                        Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getActivity(), "Please answer all the questions before submit", Toast.LENGTH_LONG).show();
                }
            }
        });
        escalate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if("ESCALATE".equalsIgnoreCase(escalate_btn.getText().toString())){
                    alllowesclatestatus ="2";
                }else{
                    alllowesclatestatus ="0";
                }
                if(selected_ques_ans.size()==10) {
                    if (nw.isConnectingToInternet()) {
                        new downloadReOtpOperation().execute();
                    } else {

                        Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getActivity(), "Please answer all the questions before submit", Toast.LENGTH_LONG).show();
                }
            }
        });
        return root;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();

        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            }

            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    public void prepareData(){
        bulker_list_qes = new ArrayList<>();
        bulker_list = new ArrayList<>();

        bulker_list.add("DRIVER");
        bulker_list.add("Driver has valid Driving Licence ");
        bulker_list.add("Driver passed Alcohol Test (Blood Alcohol level)");
        bulker_list.add("Driver has PPE (Safety Helmet, Safety Shoes and Reflective Jacket)");

        bulker_list.add("VEHICLE");
        bulker_list.add("RC Book / Fitness Certificate/ Insurance/Pollution documents available");
        bulker_list.add("Head Lights & Rear lights in working order");
        bulker_list.add("Brakes in working order");
        bulker_list.add("Hand brake in working order");
        bulker_list.add("Windscreen wipers in working order");
        bulker_list.add("All tyres (including spares) in roadworthy condition (treads not worn out)");
        bulker_list.add("No hazardous goods kept inside the Cabin (Stove / Kerosene etc.)");



        bulker_list_qes.add("Driver has valid Driving Licence");
        bulker_list_qes.add("Driver passed Alcohol Test (Blood Alcohol level)");
        bulker_list_qes.add("Driver has PPE (Safety Helmet, Safety Shoes and Reflective Jacket)");
        bulker_list_qes.add("RC Book / Fitness Certificate/ Insurance/Pollution documents available");
        bulker_list_qes.add("Head Lights & Rear lights in working order");
        bulker_list_qes.add("Brakes in working order");
        bulker_list_qes.add("Hand brake in working order");
        bulker_list_qes.add("Windscreen wipers in working order");
        bulker_list_qes.add("All tyres (including spares) in roadworthy condition (treads not worn out)");
        bulker_list_qes.add("No hazardous goods kept inside the Cabin (Stove / Kerosene etc.)");



    }




    public void selectItem(int position) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (position) {
            case 1:
                fragmentClass = VehiclePermissionFragment.class;
                break;
            default:
                fragmentClass = SafetyAuditFragment.class;
        }
        if (fragmentClass != null) {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }



    public class BulkerCheckListAdapter extends BaseAdapter {

        private Activity activity;
        private LayoutInflater inflater=null;
        ArrayList<String> bulker_list;


        public BulkerCheckListAdapter(Activity a,ArrayList<String> bulker_list) {
            activity = a;
            this.bulker_list = bulker_list;
            selectedAnswers = new ArrayList<>();
            for (int i = 0; i < bulker_list.size(); i++) {
                selectedAnswers.add("Not Attempted");
            }
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            return bulker_list.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi=convertView;
            vi= null;



            if(position==0){
                vi = inflater.inflate(R.layout.bulker_list_inner_header_items, null);
                TextView header_title = (TextView) vi.findViewById(R.id.h_title); // title
                header_title.setText(bulker_list.get(position).toString());

            }else if(position == 4){
                vi = inflater.inflate(R.layout.bulker_list_inner_header_items, null);
                TextView header_title = (TextView) vi.findViewById(R.id.h_title); // title
                header_title.setText(bulker_list.get(position).toString());

            }else {

                vi = inflater.inflate(R.layout.bulker_check_list_item, null);
                ImageView star_img = (ImageView) vi.findViewById(R.id.star_icon);
                TextView questions = (TextView) vi.findViewById(R.id.questions); // title
                RadioButton yes_button = (RadioButton) vi.findViewById(R.id.yes_option);
                RadioButton no_button = (RadioButton) vi.findViewById(R.id.no_option);

                final LinearLayout comment_lay = (LinearLayout) vi.findViewById(R.id.comment_lay);
                final EditText comment_edt = (EditText) vi.findViewById(R.id.comment_edt);

                questions.setText(bulker_list.get(position).toString());

                if(position >=0 && position <= 11){
                    star_img.setVisibility(View.VISIBLE);
                }else{
                    star_img.setVisibility(View.GONE);
                }
                yes_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                        if(checked){
                            comment_lay.setVisibility(View.GONE);
                            selectedAnswers.set(position,"yes");

                            int index = bulker_list_qes.indexOf(bulker_list.get(position).toString());
                            if(index==-1){
                                index=0;
                            }
                            selected_ques_ans.put(index+1,"yes");
                            selected_ques_ans_comm.put(index+1,"0");
                        }

                        updateButton(getCheckListStatus());

                    }
                });

                no_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                        if(checked){
                            comment_lay.setVisibility(View.VISIBLE);
                            selectedAnswers.set(position,"no");
                            int index = bulker_list_qes.indexOf(bulker_list.get(position).toString());
                            if(index==-1){
                                index=0;
                            }
                            selected_ques_ans.put(index+1,"no");
                        }
                        updateButton(getCheckListStatus());
                    }
                });

                comment_edt.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        int index = bulker_list_qes.indexOf(bulker_list.get(position).toString());
                        if(index==-1){
                            index=0;
                        }
                        selected_ques_ans_comm.put(index+1,s.toString());
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        // TODO Auto-generated method stub
                    }
                });

                if(selectedAnswers.size()!=0){
                    String value = selectedAnswers.get(position).toString();
                    if(value.equalsIgnoreCase("no")){
                        comment_lay.setVisibility(View.VISIBLE);
                        no_button.setChecked(true);

                        int index = bulker_list_qes.indexOf(bulker_list.get(position).toString());
                        if(index==-1){
                            index=0;
                        }
                        //  selected_ques_ans_comm.put(index+1,s.toString());
                        if(selected_ques_ans_comm.containsKey(index+1)){
                            String given_comment = selected_ques_ans_comm.get(index+1).toString();
                            comment_edt.setText(given_comment);
                        }
                    }else if(value.equalsIgnoreCase("yes")){
                        comment_lay.setVisibility(View.GONE);
                        yes_button.setChecked(true);
                    }
                }


            }

            return vi;
        }
    }

    public void updateButton(String value){

        if(value.equalsIgnoreCase("0")){
            reject_lay.setVisibility(View.VISIBLE);
            con_approve_lay.setVisibility(View.GONE);
            approve_lay.setVisibility(View.GONE);
            //footer_button_lay.setLayoutParams(param_1);
            allow_btn.setVisibility(View.GONE);
            escalate_btn.setVisibility(View.VISIBLE);
            escalate_btn.setText("Reject");
            allow_btn.setLayoutParams(param_2);
            escalate_btn.setLayoutParams(param_2);

        }else if(value.equalsIgnoreCase("1")){
            reject_lay.setVisibility(View.GONE);
            con_approve_lay.setVisibility(View.GONE);
            approve_lay.setVisibility(View.VISIBLE);
            //footer_button_lay.setLayoutParams(param_1);
            allow_btn.setVisibility(View.VISIBLE);
            escalate_btn.setVisibility(View.GONE);
            allow_btn.setLayoutParams(param_2);
            escalate_btn.setLayoutParams(param_2);

        }else if(value.equalsIgnoreCase("2")){
            reject_lay.setVisibility(View.GONE);
            con_approve_lay.setVisibility(View.VISIBLE);
            approve_lay.setVisibility(View.GONE);
           // footer_button_lay.setLayoutParams(param_2);
            allow_btn.setVisibility(View.VISIBLE);
            escalate_btn.setVisibility(View.VISIBLE);
            if(escbuttonshow.equalsIgnoreCase("0")){
                escalate_btn.setText("NOT ALLOW");
            }else{
                escalate_btn.setText("ESCALATE");
            }
            allow_btn.setLayoutParams(param_1);
            escalate_btn.setLayoutParams(param_1);

           /* LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(10, 0, 0, 0);
            escalate_btn.setLayoutParams(params);*/
        }


    }




    private class downloadReOtpOperation extends AsyncTask<String, Void, Void> {

        String status, message;
        JSONObject json2;
        String response,escalatedto,formtype,escalatedtoid,checklistserialno;

        @Override
        protected void onPreExecute() {
            prgDialog.setMessage("Sending...please wait.");
            prgDialog.show();
        }

        @Override
        protected Void doInBackground(String... urls) {

            if (nw.isConnectingToInternet() == true) {
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    ServiceHandler sh = new ServiceHandler();

                    nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.action, "acclssOutboundFormSubmit"));
                    nameValuePairs.add(new BasicNameValuePair("checklistserialno", ""));
                    nameValuePairs.add(new BasicNameValuePair("userid", user_id));
                    nameValuePairs.add(new BasicNameValuePair("vehicleid", vehicleid));
                    nameValuePairs.add(new BasicNameValuePair("driverid", driverid));
                    nameValuePairs.add(new BasicNameValuePair("vehicleregdno", vehicleregdno));
                    nameValuePairs.add(new BasicNameValuePair("drivername", drivername));
                    nameValuePairs.add(new BasicNameValuePair("driverdlno", driverdlno));
                    nameValuePairs.add(new BasicNameValuePair("drivermobileno", drivermobileno));
                    nameValuePairs.add(new BasicNameValuePair("transportername", transportername));
                    nameValuePairs.add(new BasicNameValuePair("transporterid", transporterid));

                    nameValuePairs.add(new BasicNameValuePair("lssrole", lssrole));
                    nameValuePairs.add(new BasicNameValuePair("lssroleid", lssroleid));

                    nameValuePairs.add(new BasicNameValuePair("plantlocationid", location_id));
                    nameValuePairs.add(new BasicNameValuePair("plantid", plantid));
                    nameValuePairs.add(new BasicNameValuePair("fcomment", final_comment.getText().toString()));


                    nameValuePairs.add(new BasicNameValuePair("chkliststatus", getCheckListStatus()));
                    nameValuePairs.add(new BasicNameValuePair("alllowesclatestatus", alllowesclatestatus));
                    nameValuePairs.add(new BasicNameValuePair("radioanswer", getRadionAns()));
                    nameValuePairs.add(new BasicNameValuePair("radioquestionID", getRadionQuesID()));
                    nameValuePairs.add(new BasicNameValuePair("radioquestioncomments", getRadionQuesComment()));

                    response = sh.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST,
                            nameValuePairs);
                    Log.e("response", response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString(AllStaticVariables.status);

                    if(alllowesclatestatus.equalsIgnoreCase("2")) {
                        escalatedtoid = js.getString("escalatedtoid");
                        escalatedto = js.getString("escalatedto");
                        formtype = js.getString("formtype");
                        checklistserialno = js.getString("checklist_id");
                    }
                    Log.e("status", status);

                    if ("0".equalsIgnoreCase(status)) {
                        message = js.getString(AllStaticVariables.msg);
                    } else {
                        message = js.getString(AllStaticVariables.msg);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                netConnection = true;
            } else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();

            if (netConnection == false) {
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                if (status.equalsIgnoreCase("1")) {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.escalatedto, escalatedto);
                    MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.escalatedtoid, escalatedtoid);
                    MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.formtype, formtype);
                    MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.checklistserialno, checklistserialno);

                    if(alllowesclatestatus.equalsIgnoreCase("2")){
                       selectItem(1);
                   }else {

                       Thread timerThread = new Thread() {
                           public void run() {
                               try {
                                   sleep(2000);
                               } catch (InterruptedException e) {
                                   e.printStackTrace();
                               } finally {
                                   selectItem(10);
                               }
                           }
                       };
                       timerThread.start();
                   }

                } else {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
            super.onPostExecute(result);
        }

    }


    public String getCheckListStatus(){

        String status="";

         for(int i =1;i<=10;i++){

             if(!status.equalsIgnoreCase("reject")) {
                 if (selected_ques_ans.containsKey(i)) {
                     if (selected_ques_ans.get(i).equalsIgnoreCase("yes")) {
                         status = "approve";
                     } else {
                         status = "reject";
                     }
                 }
             }
         }
        if(status.equalsIgnoreCase("approve")) {
            for (int i = 11; i <= 16; i++) {
                if (selected_ques_ans.containsKey(i)) {
                    if (selected_ques_ans.get(i).equalsIgnoreCase("no")) {
                        status = "con_approve";
                    }
                }
            }
        }

        if(status.equalsIgnoreCase("approve")){
            return "1";
        }else if (status.equalsIgnoreCase("reject")){
            return "0";
        }else if(status.equalsIgnoreCase("con_approve")){
            return "2";
        }else{
            return "0";
        }



    }

    public String getRadionAns(){
        StringBuilder sb = new StringBuilder();
        String prefix = "";
        for(int i =1;i <=selected_ques_ans.size();i++){

            if(selected_ques_ans.containsKey(i)){
                String ans = selected_ques_ans.get(i).toString();

                    sb.append(prefix);
                    prefix = ",";
                    sb.append(ans);

            }
        }

        return sb.toString();

    }
    public String getRadionQuesID(){

        StringBuilder sb = new StringBuilder();
        String prefix = "";

        for ( int key : selected_ques_ans.keySet() ) {

            sb.append(prefix);
            prefix = ",";
            sb.append(String.valueOf(key));

        }

        return sb.toString();
    }
    public String getRadionQuesComment(){
        StringBuilder sb = new StringBuilder();
        String prefix = "";
        for(int i =1;i <=selected_ques_ans_comm.size();i++){

            if(selected_ques_ans_comm.containsKey(i)){
                String ans = selected_ques_ans_comm.get(i).toString();

                sb.append(prefix);
                prefix = ",";
                sb.append(ans);

            }
        }

        return sb.toString();

    }

    public void keepItForEscalation(){

    }
}
