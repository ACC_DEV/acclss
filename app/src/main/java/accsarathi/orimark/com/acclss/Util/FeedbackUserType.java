package accsarathi.orimark.com.acclss.Util;

/**
 * Created by prakash on 02/08/17.
 */

public class FeedbackUserType {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    public String getFeedbackstatus() {
        return feedbackstatus;
    }

    public void setFeedbackstatus(String feedbackstatus) {
        this.feedbackstatus = feedbackstatus;
    }

    private String id;
    private String title;
    private String comments;
    private String postdate;
    private String feedbackstatus;

    public FeedbackUserType(String id, String title, String comments, String postdate, String feedbackstatus){
        this.id=id;
        this.title=title;
        this.comments=comments;
        this.postdate=postdate;
        this.feedbackstatus=feedbackstatus;

    }

}
