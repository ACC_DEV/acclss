package accsarathi.orimark.com.acclss.smsbroadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by prakash on 12/5/17.
 */

public class SmsReceiver extends BroadcastReceiver{
    private static SmsListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data  = intent.getExtras();

        Object[] pdus = (Object[]) data.get("pdus");

        for(int i=0;i<pdus.length;i++){
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
            String phoneNumber = smsMessage.getDisplayOriginatingAddress();
            String senderNum = phoneNumber;

            try {
                String asubstring = senderNum.substring(3, 9);
                if (asubstring.equals("ACCLSS")) {
                    String message = smsMessage.getDisplayMessageBody().split(":")[1];
                    message = message.substring(0, message.length()-1);
                    Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);
                    Intent myIntent = new Intent("otp");
                    myIntent.putExtra("message",message);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(myIntent);
                    // Show
                }

            } catch (Exception e) {

                e.printStackTrace() ;
            }

            //String sender = smsMessage.getDisplayOriginatingAddress();
            //String messageBody = smsMessage.getMessageBody();

        }

    }

    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }
}
