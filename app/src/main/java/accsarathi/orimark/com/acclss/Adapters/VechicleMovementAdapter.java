package accsarathi.orimark.com.acclss.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import accsarathi.orimark.com.acclss.Activities.CheckListSavedDataActivity;
import accsarathi.orimark.com.acclss.Model.VechicleMvmntModel;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.CustomItemClickListener;

/**
 * Created by star on 3/1/2018.
 */

public class VechicleMovementAdapter extends RecyclerView.Adapter<VechicleMovementAdapter.MyViewHolder> {

    private Context mContext;
    CustomItemClickListener listener;
    View mRootview;
    ArrayList<VechicleMvmntModel> all_items;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView vechicleType,txtVichleStatus, txtLocation, txtAuditestRslt, txtAuditedBy, txtLssRole, txtDate, txtTime;
        private ImageView conditionalIcon, arrowIcon;

        public MyViewHolder(View view) {
            super(view);
            mRootview = view;
            txtLocation = view.findViewById(R.id.txtLocation);
            txtAuditestRslt = view.findViewById(R.id.txtAuditestRslt);
            txtAuditedBy = view.findViewById(R.id.txtAuditedBy);
            txtLssRole = view.findViewById(R.id.txtLssRole);
            txtDate = view.findViewById(R.id.txtDate);
            txtTime = view.findViewById(R.id.txtTime);
            conditionalIcon = view.findViewById(R.id.conditionalIcon);
            arrowIcon = view.findViewById(R.id.arrowIcon);
            vechicleType = view.findViewById(R.id.vechicleType);
            txtVichleStatus = view.findViewById(R.id.txtVichleStatus);
        }
    }


    public VechicleMovementAdapter(Context mContext, ArrayList<VechicleMvmntModel> all_items) {
        this.mContext = mContext;
        this.all_items = all_items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_vechicle_mvmnt_items, parent, false);

        final MyViewHolder mViewHolder = new MyViewHolder(itemView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final VechicleMvmntModel dataModel = all_items.get(position);

        holder.txtLocation.setText(dataModel.getLocation());
        holder.txtAuditedBy.setText(dataModel.getAuditedby());
        holder.txtAuditestRslt.setText(dataModel.getAuditedresult());
        holder.txtLssRole.setText(dataModel.getLssrole());
        String str = dataModel.getDatetime();
        String[] splitStr = str.split("\\s+");
        holder.txtDate.setText(splitStr[0]);
        holder.txtTime.setText(splitStr[1]);
        holder.vechicleType.setText(dataModel.getFormtype());
        holder.txtVichleStatus.setText(dataModel.getVehiclestatus());

        if(position == 0){
            holder.arrowIcon.setVisibility(View.INVISIBLE);
        }else{
            holder.arrowIcon.setVisibility(View.VISIBLE);
        }

        if ("1".equalsIgnoreCase(dataModel.getAuditedresultstatus())) {
            holder.conditionalIcon.setBackgroundResource(R.drawable.approve);
        } else if ("2".equalsIgnoreCase(dataModel.getAuditedresultstatus())) {
            holder.conditionalIcon.setBackgroundResource(R.drawable.cond_approve);
        } else {
            holder.conditionalIcon.setBackgroundResource(R.drawable.rejected_new);
        }
        mRootview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent checklistsavData = new Intent(mContext, CheckListSavedDataActivity.class);
                checklistsavData.putExtra("checklistId", dataModel.getChecklist_id());
                mContext.startActivity(checklistsavData);
            }
        });


    }

    @Override
    public int getItemCount() {
        return all_items.size();
    }


}
