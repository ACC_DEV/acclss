package accsarathi.orimark.com.acclss.Model;

/**
 * Created by oditek on 31/01/18.
 */

public class DecisionPending {
    String id;
    String lss_roll;
    String check_list_id;
    String vehicle_reg_no;
    String location;
    String aud_by;
    String date_time;
    String status;

    public String getShowlist() {
        return showlist;
    }

    public void setShowlist(String showlist) {
        this.showlist = showlist;
    }

    String showlist;

    public String getFormtype() {
        return formtype;
    }

    public void setFormtype(String formtype) {
        this.formtype = formtype;
    }

    String formtype;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLss_roll() {
        return lss_roll;
    }

    public void setLss_roll(String lss_roll) {
        this.lss_roll = lss_roll;
    }

    public String getCheck_list_id() {
        return check_list_id;
    }

    public void setCheck_list_id(String check_list_id) {
        this.check_list_id = check_list_id;
    }

    public String getVehicle_reg_no() {
        return vehicle_reg_no;
    }

    public void setVehicle_reg_no(String vehicle_reg_no) {
        this.vehicle_reg_no = vehicle_reg_no;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAud_by() {
        return aud_by;
    }

    public void setAud_by(String aud_by) {
        this.aud_by = aud_by;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
