package accsarathi.orimark.com.acclss.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.CustomItemClickListener;

/**
 * Created by star on 3/1/2018.
 */

public class AllowListAdapter extends RecyclerView.Adapter<AllowListAdapter.MyViewHolder> {

    private Context mContext;
    CustomItemClickListener listener;

    String all_titles[];
    int all_images[];

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        public ImageView status_image, overflow;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id._title);

            status_image = (ImageView) view.findViewById(R.id.status_image);
        }
    }


    public AllowListAdapter(Context mContext, String all_titles[] , int all_images[], CustomItemClickListener listener) {
        this.mContext = mContext;
        this.all_titles = all_titles;
        this.all_images = all_images;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.allow_list_items, parent, false);
        final MyViewHolder mViewHolder = new MyViewHolder(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        //String quiz_title = all_titles[position];
        //holder.title.setText(quiz_title);
        //holder.status_image.setImageResource(all_images[position]);
       /* Album album = albumList.get(position);
        holder.title.setText(album.getName());
        holder.count.setText(album.getNumOfSongs() + " songs");

        // loading album cover using Glide library
        Glide.with(mContext).load(album.getThumbnail()).into(holder.thumbnail);

        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return 3;
    }


}
