package accsarathi.orimark.com.acclss.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import accsarathi.orimark.com.acclss.Activities.CheckListSavedDataActivity;
import accsarathi.orimark.com.acclss.Activities.HomeslidingActivity;
import accsarathi.orimark.com.acclss.Model.DecisionPendingDetails;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.MyUrl;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;
import accsarathi.orimark.com.acclss.Util.ServiceHandler;


public class DecisionPendingDetailFragment extends Fragment {

    View root;

    ImageView header_image;
    TextView header_title;
    RelativeLayout imageView_containor;
    FragmentManager fragmentManager;
    String  vehicleregdno,checklist_id;
    NetworkConnection nw;
    ProgressDialog prgDialog;
    Boolean netConnection = false;
    ArrayList<DecisionPendingDetails> decisionPendingArrayList;
    ListView decision_list;
    EditText current_status_edt,final_comment;
    Button allow_btn,not_allow_btn;
    String chkliststatus="",user_id,each_checklist_id,currentstatus,currentstatusid,escalated_id;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = null;
        root = inflater.inflate(R.layout.decision_pending_detail_page, container, false);
        nw = new NetworkConnection(getActivity());
        prgDialog = new ProgressDialog(getActivity());
        prgDialog.setCancelable(false);
        decision_list = (ListView) root.findViewById(R.id.decision_list);
        header_title = (TextView)root.findViewById(R.id.title);
        header_title.setText("Decision Pending");
        header_image =(ImageView)root.findViewById(R.id.title_img);
        imageView_containor = (RelativeLayout) root.findViewById(R.id.imageView_containor);
        imageView_containor.setVisibility(View.VISIBLE);
        header_image.setImageResource(R.drawable.decision_pending);

        View footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.decision_list_detail_footer_lay, null, false);
        current_status_edt = (EditText) footerView.findViewById(R.id.current_status_edt);
        final_comment = (EditText) footerView.findViewById(R.id.final_comment);
        allow_btn = (Button) footerView.findViewById(R.id.allow_btn);
        not_allow_btn = (Button) footerView.findViewById(R.id.not_allow_btn);


        decision_list.addFooterView(footerView);



        vehicleregdno = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.vehicleregdno);
        escalated_id = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.escalated_id);
        checklist_id = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.checklist_id);
        user_id = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.userid);
        new getDecisionPendingListDetail().execute();

        allow_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chkliststatus = "1";
                String final_comme= final_comment.getText().toString();
                if(final_comme.trim().length()==0){
                    final_comment.setError("Please write your comment");
                }else {
                    new submitDecisionPendingListDetail().execute();
                }
            }
        });
        not_allow_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chkliststatus = "0";
                String final_comme= final_comment.getText().toString();
                if(final_comme.trim().length()==0){
                    final_comment.setError("Please write your comment");
                }else {
                    new submitDecisionPendingListDetail().execute();
                }
            }
        });
        HomeslidingActivity.menu_icon.setBackgroundResource(R.drawable.left_arrow_50x50);
        HomeslidingActivity.menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectItem(10);
            }
        });
        return root;
    }
    public void selectItem(int position) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (position) {
            case 1:
                fragmentClass = CheckListSavedDataFragment.class;
                break;
            default:
                fragmentClass = DecisionPendingFragment.class;
        }
        if (fragmentClass != null) {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }

    private class getDecisionPendingListDetail extends AsyncTask<String, Void, Void> {

        String status, message;
        JSONObject json2;
        String response;
        public String id ,name;
        @Override
        protected void onPreExecute() {
            prgDialog.setMessage(getResources().getString(R.string.otp_verifiy));
            prgDialog.show();
        }

        @Override
        protected Void doInBackground(String... urls) {

            if (nw.isConnectingToInternet() == true) {
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    ServiceHandler sh = new ServiceHandler();
                    nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.action, "acclssvehiclechecklist"));
                    nameValuePairs.add(new BasicNameValuePair("vehicleregdno", vehicleregdno));
                    nameValuePairs.add(new BasicNameValuePair("checklist_id", checklist_id));
                    nameValuePairs.add(new BasicNameValuePair("escalateid", escalated_id));

                    response = sh.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST,
                            nameValuePairs);
                    Log.e("response", response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString(AllStaticVariables.status);
                    each_checklist_id = js.getString("checklist_id");
                    currentstatus = js.getString("currentstatus");
                    currentstatusid = js.getString("currentstatusid");
                    if(status.equalsIgnoreCase("1")) {
                        decisionPendingArrayList = new ArrayList<>();
                        JSONArray jsonArray = js.getJSONArray(AllStaticVariables.data);
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject all_details = jsonArray.getJSONObject(i);
                            DecisionPendingDetails decisionPending = new DecisionPendingDetails();

                            decisionPending.setId(all_details.getString("id").trim());
                            decisionPending.setCheck_list_id(all_details.getString("checklist_id").trim());
                            decisionPending.setLss_roll(all_details.getString("lssrole").trim());
                            decisionPending.setVehicle_reg_no(all_details.getString("vehicleregdno").trim());
                            decisionPending.setLocation(all_details.getString("location").trim());
                            decisionPending.setAud_by(all_details.getString("auditedby").trim());
                            decisionPending.setDate_time(all_details.getString("datetime").trim());
                            decisionPending.setStatus(all_details.getString("auditedresult").trim());
                            decisionPending.setFormtype(all_details.getString("formtype").trim());
                            decisionPending.setIncValue(all_details.getString("incValue"));
                            decisionPending.setAuditedresultstatus(all_details.getString("auditedresultstatus"));
                            decisionPendingArrayList.add(decisionPending);

                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                netConnection = true;
            } else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();

            if (netConnection == false) {
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                if (status.equalsIgnoreCase("1")) {

                    current_status_edt.setText(currentstatus);
                    Collections.sort(decisionPendingArrayList, new Comparator<DecisionPendingDetails>() {
                        public int compare(DecisionPendingDetails obj1, DecisionPendingDetails obj2) {
                            return obj2.getIncValue().compareToIgnoreCase(obj1.getIncValue());
                        }
                    });
                    DecisionPendingDetailAdapter decisionPendingDetailAdapter = new DecisionPendingDetailAdapter(getActivity(),decisionPendingArrayList);
                    decision_list.setAdapter(decisionPendingDetailAdapter);
                }else{
                    Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.no_records), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
            super.onPostExecute(result);
        }

    }


    public class DecisionPendingDetailAdapter extends BaseAdapter {

        private Activity activity;
        private LayoutInflater inflater=null;
        ArrayList<DecisionPendingDetails> pending_detail_list;
        public TextView aud_res_txt, loc_txt,aud_by_txt,date_time_txt,lss_role_edt,vechicleType;
        public ImageView status_image,arrow_image;
        private RelativeLayout cardframe;


        public DecisionPendingDetailAdapter(Activity a,ArrayList<DecisionPendingDetails> pending_detail_list) {
            activity = a;
            this.pending_detail_list = pending_detail_list;

            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            return pending_detail_list.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            View view=convertView;
            view= null;
            view = inflater.inflate(R.layout.decision_pending_detail_list_item, null);
            aud_res_txt = (TextView)view.findViewById(R.id.aud_res_edt);
            loc_txt = (TextView) view.findViewById(R.id.loc_edt);
            aud_by_txt = (TextView) view.findViewById(R.id.aud_by_edt);
            date_time_txt = (TextView) view.findViewById(R.id.date_edt);
            lss_role_edt = (TextView) view.findViewById(R.id.lss_role_edt);
            vechicleType= (TextView) view.findViewById(R.id.vechicleType);
            status_image = (ImageView) view.findViewById(R.id.status_img);
            arrow_image = (ImageView) view.findViewById(R.id.arrow_image);
            cardframe = view.findViewById(R.id.cardframe);

            aud_res_txt.setText(pending_detail_list.get(position).getStatus());
            loc_txt.setText(pending_detail_list.get(position).getLocation());
            aud_by_txt.setText(pending_detail_list.get(position).getAud_by());
            date_time_txt.setText(pending_detail_list.get(position).getDate_time());
            lss_role_edt.setText(pending_detail_list.get(position).getLss_roll());
            vechicleType.setText(pending_detail_list.get(position).getFormtype());

           /* if(pending_detail_list.get(position).getStatus().equalsIgnoreCase("Approve")){
                status_image.setImageResource(R.drawable.approve);
            }else if(pending_detail_list.get(position).getStatus().equalsIgnoreCase("Conditional Approve")){
                status_image.setImageResource(R.drawable.cond_approve);
            }else if(pending_detail_list.get(position).getStatus().equalsIgnoreCase("Reject")){
                status_image.setImageResource(R.drawable.rejected);
            }*/

            if ("1".equalsIgnoreCase(pending_detail_list.get(position).getAuditedresultstatus())) {
                status_image.setBackgroundResource(R.drawable.approve);
            } else if ("2".equalsIgnoreCase(pending_detail_list.get(position).getAuditedresultstatus())) {
                status_image.setBackgroundResource(R.drawable.cond_approve);
            } else {
                status_image.setBackgroundResource(R.drawable.rejected_new);
            }


            if(position == 0){
                arrow_image.setVisibility(View.INVISIBLE);
            }else{
                arrow_image.setVisibility(View.VISIBLE);
            }

            cardframe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent checklistsavData= new Intent(getActivity(), CheckListSavedDataActivity.class);
                    checklistsavData.putExtra("checklistId",pending_detail_list.get(position).getCheck_list_id());
                    getActivity().startActivity(checklistsavData);

                }
            });

            return view;
        }
    }

    private class submitDecisionPendingListDetail extends AsyncTask<String, Void, Void> {

        String status, message;
        JSONObject json2;
        String response;
        @Override
        protected void onPreExecute() {
            prgDialog.setMessage(getResources().getString(R.string.otp_verifiy));
            prgDialog.show();
        }

        @Override
        protected Void doInBackground(String... urls) {

            if (nw.isConnectingToInternet() == true) {
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    ServiceHandler sh = new ServiceHandler();
                    nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.action, "acclssvehiclestatusupdate"));
                    nameValuePairs.add(new BasicNameValuePair("checklist_id", each_checklist_id));
                    nameValuePairs.add(new BasicNameValuePair("chkliststatus", chkliststatus));
                    nameValuePairs.add(new BasicNameValuePair("userid",user_id));
                    nameValuePairs.add(new BasicNameValuePair("currentstatus", currentstatusid));
                    nameValuePairs.add(new BasicNameValuePair("comments", final_comment.getText().toString()));
                    nameValuePairs.add(new BasicNameValuePair("escalateid", escalated_id));

                  //  checklist_id,chkliststatus,userid,currentstatus,comments
                    response = sh.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST,
                            nameValuePairs);
                    Log.e("response", response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString(AllStaticVariables.status);
                    if (status.equalsIgnoreCase("1")) {
                        message = js.getString(AllStaticVariables.msg);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                netConnection = true;
            } else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();

            if (netConnection == false) {
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                if (status.equalsIgnoreCase("1")) {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    selectItem(10);

                }else{
                    Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.no_records), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
            super.onPostExecute(result);
        }

    }

}
