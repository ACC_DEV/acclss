package accsarathi.orimark.com.acclss.Fragments;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import accsarathi.orimark.com.acclss.Activities.HomeslidingActivity;
import accsarathi.orimark.com.acclss.Adapters.VehicleStatusAdapter;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.CustomItemClickListener;
import accsarathi.orimark.com.acclss.Util.MyUrl;

import static android.content.ContentValues.TAG;


public class LandingFragment extends Fragment {

    View root;
    RecyclerView vehicle_status_list;
    String all_titles[] ={"Safety Audits","Decision Pending","Rejected","Allowed","Vehicle Movement","Repair Status"};
    int all_images[] = {R.drawable.status_audit,R.drawable.decision_pending,R.drawable.rejected,R.drawable.allowed,R.drawable.vehicle_movement,R.drawable.repair_status};
    public static LandingFragment newInstance() {
        return new LandingFragment();
    }
    ImageView header_image;
    TextView header_title;
    FragmentManager fragmentManager;
    RelativeLayout imageView_containor;
    String lss_roll,name,user_id;
    EditText name_edt,roll_edt;
    private String rCount="0",dCount="0",aCount="0";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = null;
        root = inflater.inflate(R.layout.landing_page, container, false);
        vehicle_status_list = (RecyclerView) root.findViewById(R.id.vehicle_status_list);
        header_title = (TextView)root.findViewById(R.id.title);
     //   imageView_containor = (RelativeLayout) root.findViewById(R.id.imageView_containor);
        header_title.setText("Vehicle Safety Monitoring System");
        header_image =(ImageView)root.findViewById(R.id.title_img);
//        imageView_containor.setVisibility(View.INVISIBLE);
        name_edt = root.findViewById(R.id.name_edt);
        roll_edt = root.findViewById(R.id.lcc_role_edt);
        name = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.username);
        lss_roll = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.lssrole);
        user_id = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.userid);
        name_edt.setText(name);
        roll_edt.setText(lss_roll);


        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("reject_alert"));

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("allow_alert"));

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("decision_alert"));

        NotificationManager notifManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.cancelAll();

            rCount = MyUrl.getNotoDataFromKey(getActivity(),"reject_noto_count");
            aCount = MyUrl.getNotoDataFromKey(getActivity(),"allow_noto_count");
            dCount = MyUrl.getNotoDataFromKey(getActivity(),"decision_noto_count");

        setListData(rCount,aCount,dCount);

        HomeslidingActivity.menu_icon.setBackgroundResource(R.mipmap.menu_bar);
        return root;
    }

    public void selectItem(int position) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (position) {
            case 0:
                fragmentClass = SafetyAuditFragment.class;
                break;
            case 1:
                fragmentClass = DecisionPendingFragment.class;
                break;

            case 2:
                fragmentClass = RejectedFragment.class;
                break;

            case 3:
                fragmentClass = AllowFragment.class;
                break;
            case 4:
                fragmentClass = VehicleMovementFragment.class;
                break;
            case 5:
                fragmentClass = RepairFragment.class;
                break;
            default:
                // fragmentClass = LandingFragment.class;
        }
        if (fragmentClass != null) {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }
    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try{
            String type = intent.getStringExtra("type");
            getNotoReadStatus(type);
            NotificationManager notifManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notifManager.cancelAll();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    public void getNotoReadStatus(String type) {
      try{
        if(type.equalsIgnoreCase("0")){
            rCount = MyUrl.getNotoDataFromKey(getActivity(),"reject_noto_count");
        }else if(type.equalsIgnoreCase("1")){
            aCount = MyUrl.getNotoDataFromKey(getActivity(),"allow_noto_count");
        }else if(type.equalsIgnoreCase("2")){
            dCount = MyUrl.getNotoDataFromKey(getActivity(),"decision_noto_count");
        }

        setListData(rCount,aCount,dCount);

      }catch (Exception e){
          e.printStackTrace();
      }
    }

    public void setListData(String rCount,String aCount,String dCount){
        VehicleStatusAdapter adapter = new VehicleStatusAdapter(getActivity(), all_titles,all_images,rCount,aCount,dCount, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Log.d(TAG, "clicked position:" + position);
                // String quiz_title = (new ArrayList<>(all_quizzes.keySet())).get(position);
                selectItem(position);

            }
        });
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        vehicle_status_list.setLayoutManager(mLayoutManager);
        vehicle_status_list.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(2), true));
        vehicle_status_list.setItemAnimator(new DefaultItemAnimator());
        vehicle_status_list.setAdapter(adapter);

    }

}
