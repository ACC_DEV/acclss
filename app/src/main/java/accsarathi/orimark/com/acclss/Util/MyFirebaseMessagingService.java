package accsarathi.orimark.com.acclss.Util;

/**
 * Created by oditek on 05/02/18.
 */

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.List;

import accsarathi.orimark.com.acclss.Activities.HomeslidingActivity;
import accsarathi.orimark.com.acclss.Activities.LoginActivity;
import accsarathi.orimark.com.acclss.Fragments.AllowFragment;
import accsarathi.orimark.com.acclss.Fragments.DecisionPendingFragment;
import accsarathi.orimark.com.acclss.Fragments.RejectedFragment;
import accsarathi.orimark.com.acclss.Fragments.RepairFragment;
import accsarathi.orimark.com.acclss.Fragments.SafetyAuditFragment;
import accsarathi.orimark.com.acclss.Fragments.VehicleMovementFragment;
import accsarathi.orimark.com.acclss.Model.DecisionPending;
import accsarathi.orimark.com.acclss.R;

/**
 * Created by Belal on 12/8/2017.
 */

//class extending FirebaseMessagingService
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private String Classname = null;
    int count=0;
    String myCount;
    private final int ID = 1;
    private static final String TAG = "MyFirebaseMsgService";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        //{"mode":"Alert","title":"Vehicle Allowed","message":"test"}}
        sendMessageNotification(remoteMessage.getData().get("body"));
    }
    private void sendMessageNotification(String messageBody) {
        try {
            JSONObject json = new JSONObject(messageBody);
            Log.e("Json", json.toString());
            String mode = json.getString("mode");
            String title = json.getString("title");
            String message = json.getString("message");
            String type = json.getString("type");

            Log.e("mode", mode);

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
                List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                ComponentName componentInfo = taskInfo.get(0).topActivity;
                Classname = componentInfo.getClassName();
            } else {
                getProcessOld();
            }

            if (type.equals("0")) {

                String reject_count_ = MyUrl.getNotoDataFromKey(this, "reject_noto_count");

                count = Integer.parseInt(reject_count_) + 1;
                myCount = Integer.toString(count);
                Log.e("firebase noto::", reject_count_);
                MyUrl.saveNotoDataWithKeyAndValue(this, "reject_noto_count", myCount);

                if (Classname.equals("accsarathi.orimark.com.acclss.Activities.HomeslidingActivity")) {
                    Intent alertIntent = new Intent("reject_alert");
                    alertIntent.putExtra("class_name", Classname);
                    alertIntent.putExtra("type", type);
                    alertIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    alertIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(alertIntent);
                } else {
                        sendNotification(message, title,type);

                }
            } else if (type.equals("1")) {

                String allow_count_ = MyUrl.getNotoDataFromKey(this, "allow_noto_count");

                count = Integer.parseInt(allow_count_) + 1;
                myCount = Integer.toString(count);
                Log.e("firebase noto::", allow_count_);
                MyUrl.saveNotoDataWithKeyAndValue(this, "allow_noto_count", myCount);

                if (Classname.equals("accsarathi.orimark.com.acclss.Activities.HomeslidingActivity")) {
                    Intent alertIntent = new Intent("allow_alert");
                    alertIntent.putExtra("class_name", Classname);
                    alertIntent.putExtra("type", type);
                    alertIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    alertIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(alertIntent);
                } else {
                    sendNotification(message, title,type);

                }

            }
            else if (type.equals("2")) {

                String decision_count_ = MyUrl.getNotoDataFromKey(this, "decision_noto_count");

                count = Integer.parseInt(decision_count_) + 1;
                myCount = Integer.toString(count);
                Log.e("firebase noto::", decision_count_);
                MyUrl.saveNotoDataWithKeyAndValue(this, "decision_noto_count", myCount);

                if (Classname.equals("accsarathi.orimark.com.acclss.Activities.HomeslidingActivity")) {
                    Intent alertIntent = new Intent("decision_alert");
                    alertIntent.putExtra("class_name", Classname);
                    alertIntent.putExtra("type", type);
                    alertIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    alertIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(alertIntent);
                } else {
                    sendNotification(message, title,type);

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //API below 21
    @SuppressWarnings("deprecation")
    private String getProcessOld() throws Exception {
        ActivityManager activity = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTask = activity.getRunningTasks(1);
        if (runningTask != null) {
            ActivityManager.RunningTaskInfo taskTop = runningTask.get(0);
            ComponentName componentTop = taskTop.topActivity;
            Classname = componentTop.getClassName();
        }
        return Classname;
    }
    private void sendNotification(String message,String title,String type) {
        Log.e("TYPE::", type);
        String isUserLogin= MyUrl.getDataFromKey(this, "isLogin");
        Intent intent =null;
        if(isUserLogin!=null && isUserLogin.equals("yes")){
             intent = new Intent(this, HomeslidingActivity.class);
        }else{
            intent = new Intent(this, LoginActivity.class);
        }

        intent.putExtra("type", type);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
       // Bitmap bitmap = getBitmapfromUrl(image);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
               // .setLargeIcon(bitmap)
                .setSmallIcon(R.mipmap.ic_app_noto_icon)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

}
