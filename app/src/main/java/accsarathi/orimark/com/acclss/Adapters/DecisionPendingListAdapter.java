package accsarathi.orimark.com.acclss.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import accsarathi.orimark.com.acclss.Model.DecisionPending;
import accsarathi.orimark.com.acclss.Model.RejectedModel;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.CustomItemClickListener;

/**
 * Created by star on 3/1/2018.
 */

public class DecisionPendingListAdapter extends RecyclerView.Adapter<DecisionPendingListAdapter.MyViewHolder> implements Filterable{

    private Context mContext;
    CustomItemClickListener listener;

    List<DecisionPending> arrayList;

    List<DecisionPending> arrayList_filter;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView vehicle_reg_no, loc_txt, aud_by_txt, date_time_txt, status_txt, vechicleType,lss_roll_txt;

        public MyViewHolder(View view) {
            super(view);
            vehicle_reg_no = (TextView) view.findViewById(R.id.reg_no);
            loc_txt = (TextView) view.findViewById(R.id.loc_txt);
            aud_by_txt = (TextView) view.findViewById(R.id.aud_by_txt);
            date_time_txt = (TextView) view.findViewById(R.id.date_time_txt);
            status_txt = (TextView) view.findViewById(R.id.status_txt);
            vechicleType = (TextView) view.findViewById(R.id.vechicleType);
            lss_roll_txt= (TextView) view.findViewById(R.id.lss_roll_txt);
        }
    }


    public DecisionPendingListAdapter(Context mContext, ArrayList<DecisionPending> arrayList, CustomItemClickListener listener) {
        this.mContext = mContext;
        this.arrayList = arrayList;
        this.listener = listener;
        this.arrayList_filter = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.decision_pending_list_items, parent, false);
        final MyViewHolder mViewHolder = new MyViewHolder(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {


        holder.vehicle_reg_no.setText("Vehicle Regd No :"+arrayList_filter.get(position).getVehicle_reg_no());
        holder.loc_txt.setText(arrayList_filter.get(position).getLocation());
        holder.aud_by_txt.setText(arrayList_filter.get(position).getAud_by());
        holder.date_time_txt.setText(arrayList_filter.get(position).getDate_time());
        holder.status_txt.setText(arrayList_filter.get(position).getStatus());
        holder.vechicleType.setText(arrayList_filter.get(position).getFormtype());
        holder.lss_roll_txt.setText(arrayList_filter.get(position).getLss_roll());

    }

    @Override
    public int getItemCount() {
        return arrayList_filter.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    arrayList_filter = arrayList;
                } else {
                    List<DecisionPending> filteredList = new ArrayList<>();
                    for (DecisionPending row : arrayList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getVehicle_reg_no().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    arrayList_filter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = arrayList_filter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                arrayList_filter = (ArrayList<DecisionPending>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
