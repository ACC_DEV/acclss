package accsarathi.orimark.com.acclss.Model;

/**
 * Created by prakash on 2/3/18.
 */

public class VechicleMvmntModel {
    public String getVehiclestatus() {
        return vehiclestatus;
    }

    public void setVehiclestatus(String vehiclestatus) {
        this.vehiclestatus = vehiclestatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChecklist_id() {
        return checklist_id;
    }

    public void setChecklist_id(String checklist_id) {
        this.checklist_id = checklist_id;
    }

    public String getVehicleregdno() {
        return vehicleregdno;
    }

    public void setVehicleregdno(String vehicleregdno) {
        this.vehicleregdno = vehicleregdno;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAuditedby() {
        return auditedby;
    }

    public void setAuditedby(String auditedby) {
        this.auditedby = auditedby;
    }

    public String getLssrole() {
        return lssrole;
    }

    public void setLssrole(String lssrole) {
        this.lssrole = lssrole;
    }

    public String getFcomment() {
        return fcomment;
    }

    public void setFcomment(String fcomment) {
        this.fcomment = fcomment;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getFormtype() {
        return formtype;
    }

    public void setFormtype(String formtype) {
        this.formtype = formtype;
    }

    public String getAuditedresult() {
        return auditedresult;
    }

    public void setAuditedresult(String auditedresult) {
        this.auditedresult = auditedresult;
    }

    public String getAuditedresultstatus() {
        return auditedresultstatus;
    }

    public void setAuditedresultstatus(String auditedresultstatus) {
        this.auditedresultstatus = auditedresultstatus;
    }

    private String id;
    private String checklist_id;
    private String vehicleregdno;
    private String location;
    private String auditedby;
    private String lssrole;
    private String fcomment;
    private String datetime;
    private String formtype;
    private String auditedresult;
    private String auditedresultstatus;
    private String vehiclestatus;
    public String getIncValue() {
        return incValue;
    }

    public void setIncValue(String incValue) {
        this.incValue = incValue;
    }

    private String incValue;

    public VechicleMvmntModel(String id, String checklist_id, String vehicleregdno, String location, String auditedby, String lssrole,
                              String fcomment, String datetime, String formtype, String auditedresult, String auditedresultstatus, String vehiclestatus,String incValue) {
        this.id = id;
        this.checklist_id = checklist_id;
        this.vehicleregdno = vehicleregdno;
        this.location = location;
        this.auditedby = auditedby;
        this.lssrole = lssrole;
        this.fcomment = fcomment;
        this.datetime = datetime;
        this.formtype = formtype;
        this.auditedresult = auditedresult;
        this.auditedresultstatus = auditedresultstatus;
        this.vehiclestatus = vehiclestatus;
        this.incValue = incValue;

    }


}
