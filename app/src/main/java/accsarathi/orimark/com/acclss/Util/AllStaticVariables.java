package accsarathi.orimark.com.acclss.Util;


public class AllStaticVariables {
    public static String itemid = "itemid";
    public static String action = "action";
    public static String type = "type";
    public static String userid = "userid";
    public static String pollID = "pollID";
    public static String poolid = "poolid";
    public static String company_id = "company_id";
    public static String newarrayRadioQue = "newarrayRadioQue";
    public static String newarrayAnsid = "newarrayAnsid";
    public static String optradios = "optradios";
    public static String email = "email";
    public static String id = "id";
    public static String mobno = "mobno";
    public static String mobileno = "mobileno";
    public static String otp = "otp";
    public static String checklist_id = "checklist_id";
    public static String company_profile_id = "company_profile_id";
    public static String user_business_id = "user_business_id";
    public static String product_id = "product_id";
    public static String service_id = "service_id";
    public static String location = "location";
    public static String keyword = "keyword";
    public static String NA = "NA";
    public static String data = "data";
    public static String status = "status";
    public static String msg = "msg";
    public static String feedbackType = "feedbackType";
    public static String logincode = "logincode";
    public static String token = "token";
    public static String vpassportno="vpassportno";



    public static final String CHANNEL_ID = "my_channel_01";
    public static final String CHANNEL_NAME = "Simplified Coding Notification";
    public static final String CHANNEL_DESCRIPTION = "www.simplifiedcoding.net";
    public static String FIREBASE_TOKEN = "FIREBASE_TOKEN";





    public static class loginVariable {
        public static String oauth_provider = "oauth_provider";
        public static String name = "name";
        public static String mobile = "mobile";
        public static String action = "action";
        public static String mobileno = "mobileno";
        public static String password = "password";
        public static String oauth_uid = "oauth_uid";
        public static String facebook = "facebook";
        public static String google = "google";
        public static String login = "login";
        public static String signup = "signup";
        public static String normal = "normal";
        public static String sociallogin = "sociallogin";
        public static String device_id = "device_id";
        public static String device_type = "device_type";
        public static String Android = "Android";
        // json value
        public static String id = "registration_id";
        public static String token_id = "token_id";

    }

    public static class lookforVariable {
        //action value
        public static String items = "items";
        public static String location = "location";
        public static String subcatid = "subcatid";
        public static String lookfor_categories = "lookfor_categories";
        public static String allcity = "allcity";
        public static String locationList = "locationList";
        public static String nonLoadingallcity="nonLoadingallcity";
    }

    public static class jsonObject {
        public static String status = "status";
        public static String apikey = "APIKEY";
        public static String msg = "msg";
        public static String otp = "otp";
        public static String price = "price";

    }

    public static class signupVariable {

        public static String login_type = "login_type";
        // public static String empid = "user_countrycode";
        public static String otp = "otp_code";
        public static String name = "name";
        public static String action = "action";
        public static String email = "email";
        public static String mobile = "mobileno";
        public static String password = "password";
        public static String device_id = "device_id";
        public static String department = "department";
        public static String device_type = "device_type";
        public static String Android = "Android";
        public static String location = "location";

    }


    public static class apiAction {
        public static String signup = "signup";
        public static String signin = "signin";
        public static String socialsignin = "socialsignin";
        public static String socialsignup = "socialsignup";
        public static String forgetpass = "acclssforget";
        public static String alleventscurrent = "alleventscurrent";
        public static String itemdetails = "itemdetails";
        public static String setfavourite = "setfavourite";
        public static String allcategories = "allcategories";
        public static String userFeedbackFormSubmit = "userFeedbackFormSubmit";
        public static String userMyFeedbackView = "userMyFeedbackView";
        public static String getfavourite = "getfavourite";
        public static String deletefavourite = "deletefavourite";
        public static String userViewCoupon = "userViewCoupon";
        public static String search = "search";
        public static String subcategories = "subcategories";
        public static String verifyotp = "acclssverifyotp";
        public static String resentotp = "resentotp";
        public static String autocomplete = "autocomplete";
        public static String autocompletedata = "autocompletedata";
        public static String getlocation = "getlocation";
        public static String getilike = "getilike";
        public static String setilike = "setilike";
        public static String getcatpoll = "getcatpoll";
        public static String userProfile = "userProfile";
        public static String sendpractice = "sendpractice";
        public static String checkLoginStatus = "checkLoginStatus";
        public static String logincodecheck = "logincodecheck";
        public static String logout = "logout";
        public static String tokenregister = "tokenregister";
        public static String displayCompanyWithinLocation = "displayCompanyWithinLocation";
        public static String allhsupdates = "allhsupdates";
        public static String reOTP = "reOTP";
        public static String OTP = "OTP";
        public static String setpollquestions = "setpollquestions";
        public static String submitPollAnswer = "submitPollAnswer";
        public static String userViewFeedbackNoti = "userViewFeedbackNoti";
        public static String allbestpracticelist = "allbestpracticelist";
        public static String userFeedbackForm = "userFeedbackForm";
        public static String userMyFeedback = "userMyFeedback";
        public static String sendenquiry = "sendenquiry";
        public static String allbestpractice = "allbestpractice";
        public static String filter = "filter";
        public static String reject_list = "reject_list";
        public static String allowed_list = "allowed_list";
        public static String getSaveCheckList = "getSaveCheckList";
        public static String chkVechiclePssprt="chkVechiclePssprt";

        public static String repair_list = "repair_list";
        public static String acclssrepairevehicle = "acclssrepairevehicle";


    }

    public static String company = "company";
    public static String product = "product";
    public static String service = "service";
    public static String clicktype = "clicktype";
    public static String lookfor = "lookfor";
    public static String name = "name";
    public static String itemId = "itemId";
    public static String itemLogo = "itemLogo";
    public static String itemDescription = "itemDescription";
    public static String itemBanner = "itemBanner";
    public static String isAddtofavourite = "isAddtofavourite";
    public static String feedback_id = "feedback_id";

    public static class Search {
        public static String type = "type";
        public static String id = "id";
        public static String name = "name";
        public static String address = "address";
        public static String logo = "logo";
        public static String overallrating = "overallrating";
        public static String totalreviews = "totalreviews";
        public static String favourite = "favourite";
        public static String cat_name = "cat_name";
        public static String description = "description";
        public static String banner_img = "banner_img";

    }

    public static class viewFeedback {
        public static String type = "type";
        public static String name = "name";
        public static String comment = "comment";
        public static String comnt_type = "comnt_type";
        public static String highlights = "highlights";
        public static String improvement = "improvement";
        public static String overalPoints = "overalPoints";
        public static String product_name = "product_name";
        public static String service_name = "service_name";
        public static String introduction = "introduction";
        public static String introtitle = "introtitle";
        public static String introanswer = "introanswer";
        public static String question = "question";
        public static String type_name = "type_name";
        public static String queries = "queries";
        public static String mainQuestion = "mainQuestion";
        public static String answer = "answer";
        public static String orginal_scale = "orginal_scale";
        public static String edate = "edate";
        public static String location = "location";
        public static String department = "department";

    }

    public static class Favourite {
        public static String fav_company = "fav_company";
        public static String fav_product = "fav_product";
        public static String fav_service = "fav_service";
        public static String type = "type";
        public static String id = "id";
        public static String name = "name";
        public static String address = "address";
        public static String favourite = "favourite";
        public static String logo = "logo";
        public static String banner_img = "banner_img";
        public static String description = "description";
        public static String overallrating = "overallrating";

    }

    public static class coupon_deal {
        public static String coupon = "coupon";
        public static String email = "email";
        public static String image = "image";
        public static String subject = "subject";
        public static String message = "message";
        public static String type = "type";
        public static String company_id = "company_id";
        public static String company_name = "company_name";
        public static String download_button = "download_button";
        public static String attchment = "attchment";
    }

    public static class feedback_request {
        public static String image = "image";
        public static String user_id = "user_id";
        public static String company_id = "company_id";
        public static String company_name = "company_name";
        public static String feedback_id = "feedback_id";
        public static String subject = "subject";
        public static String type = "type";
        public static String address = "address";
    }

    public static class mySharedpreference {
        public static String userid = "userid";
        public static String username = "username";
        public static String useremail = "useremail";
        public static String social_profile_image = "social_profile_image";
        public static String profile_pic = "profile_pic";
        public static String location = "location";
        public static String department = "department";
        public static String locationid = "locationid";
        //login
        public static String mobno = "mobno";
        public static String lssrole = "lssrole";
        public static String lssroleid = "lssroleid";
        public static String pwd = "pwd";
        public static String type = "type";
        public static String normal_login = "normal_login";
        public static String isLogin = "isLogin";
        public static String login_code = "login_code";
        //fcm
        public static String tokenId = "tokenId";

        /////////////////
        public static String vehicleid = "vehicleid";
        public static String driverid = "driverid";
        public static String vehicleregdno = "vehicleregdno";
        public static String payloadcapacity = "payloadcapacity";
        public static String drivername = "drivername";
        public static String driverdlno = "driverdlno";
        public static String drivermobileno = "drivermobileno";
        public static String transportername = "transportername";
        public static String transporterid = "transporterid";
        public static String transportcontractorsupervisor = "transportcontractorsupervisor";
        public static String escalatedto = "escalatedto";
        public static String plantid = "plantid";
        public static String escalatedtoid = "escalatedtoid";
        public static String formtype = "formtype";
        public static String checklistserialno = "checklistserialno";

        public static String checklist_id = "checklist_id";
        public static String selected_location = "selected_location";
        public static String selected_location_id = "selected_location_id";
        public static String escalated_id="escalated_id";

        public static String drvrpassportno = "drvrpassportno";
        public static String vpassportno="vpassportno";
        public static String escbuttonshow="escbuttonshow";

    }

    public static class JsonObj {
        public static String login_code = "login_code";
        public static String products = "products";
        public static String services = "services";
        public static String feedback = "feedback";
        public static String id = "id";
        public static String name = "name";
        public static String logo = "logo";
        public static String banner_img = "banner_img";
        public static String address = "address";
        public static String favourite = "favourite";
        public static String totalfavourite = "totalfavourite";
        public static String overallrating = "overallrating";
        public static String totalreviews = "totalreviews";
        public static String pole_id = "pole_id";
        public static String verified = "verified";
        public static String feedback_button = "feedback_button";
        public static String shareurl = "shareurl";
        public static String description = "description";
        public static String image = "image";

        public static String Profileimage = "Profileimage";
        public static String rating = "rating";
        public static String date = "date";
        public static String comments = "comments";

        public static String companyid = "companyid";
        public static String companyname = "companyname";
        public static String companyfavorite = "companyfavorite";

    }

    public static class enquiry {
        public static String businessid = "businessid";
        public static String productid = "productid";
        public static String serviceid = "serviceid";
        public static String attachment = "attachment";
        public static String subject = "subject";
        public static String message = "message";


    }

    public static class mapObj {
        public static String company_profile_id = "company_profile_id";
        public static String company_name = "company_name";
        public static String email = "email";
        public static String fxphone = "fxphone";
        public static String company_logo = "company_logo";
        public static String address = "address";
        public static String state = "state";
        public static String city = "city";
        public static String zip = "zip";
        public static String company_banner = "company_banner";
        public static String latitude = "latitude";
        public static String longitude = "longitude";
        public static String distance = "distance";
        public static String description = "description";
        public static String favourite = "favourite";
        public static String cat_name = "cat_name";
        public static String overallrating = "overallrating";
        public static String totalreviews = "totalreviews";
    }

    public static class bundleValue {
        public static String event_name = "event_name";
        public static String type = "type";
        public static String image = "image";
        public static String service_id = "service_id";
        public static String ecatid = "ecatid";
        public static String esubcatid = "esubcatid";
        public static String company_profile_id = "company_profile_id";
        public static String product_id = "product_id";
        public static String location_type = "location_type";
    }

    public static class myParams {
        public static String match = "match";
    }
}