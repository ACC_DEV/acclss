package accsarathi.orimark.com.acclss.Model;

/**
 * Created by oditek on 05/02/18.
 */

public class RepairModel {
    String id,checklist_id,vehicleregdno,location,auditedby,lssrole,datetime,checkliststatus,formtype,formTYpeId,transportername,vehiclestatus,fcomment,repairstatus;

    public RepairModel(String id, String checklist_id, String vehicleregdno, String location, String auditedby,
                       String lssrole, String datetime, String checkliststatus, String formtype, String formTYpeId, String transportername,
                       String vehiclestatus, String fcomment,String repairstatus) {
        this.id = id;
        this.checklist_id = checklist_id;
        this.vehicleregdno = vehicleregdno;
        this.location = location;
        this.auditedby = auditedby;
        this.lssrole = lssrole;
        this.datetime = datetime;
        this.checkliststatus = checkliststatus;
        this.formtype = formtype;

        this.formTYpeId = formTYpeId;
        this.transportername = transportername;

        this.vehiclestatus = vehiclestatus;
        this.fcomment = fcomment;

        this.repairstatus = repairstatus;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChecklist_id() {
        return checklist_id;
    }

    public void setChecklist_id(String checklist_id) {
        this.checklist_id = checklist_id;
    }

    public String getVehicleregdno() {
        return vehicleregdno;
    }

    public void setVehicleregdno(String vehicleregdno) {
        this.vehicleregdno = vehicleregdno;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAuditedby() {
        return auditedby;
    }

    public void setAuditedby(String auditedby) {
        this.auditedby = auditedby;
    }

    public String getLssrole() {
        return lssrole;
    }

    public void setLssrole(String lssrole) {
        this.lssrole = lssrole;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getCheckliststatus() {
        return checkliststatus;
    }

    public void setCheckliststatus(String checkliststatus) {
        this.checkliststatus = checkliststatus;
    }

    public String getFormtype() {
        return formtype;
    }

    public void setFormtype(String formtype) {
        this.formtype = formtype;
    }

    public String getFormTYpeId() {
        return formTYpeId;
    }

    public void setFormTYpeId(String formTYpeId) {
        this.formTYpeId = formTYpeId;
    }

    public String getTransportername() {
        return transportername;
    }

    public void setTransportername(String transportername) {
        this.transportername = transportername;
    }

    public String getVehiclestatus() {
        return vehiclestatus;
    }

    public void setVehiclestatus(String vehiclestatus) {
        this.vehiclestatus = vehiclestatus;
    }

    public String getFcomment() {
        return fcomment;
    }

    public void setFcomment(String fcomment) {
        this.fcomment = fcomment;
    }

    public String getRepairstatus() {
        return repairstatus;
    }

    public void setRepairstatus(String repairstatus) {
        this.repairstatus = repairstatus;
    }
}
