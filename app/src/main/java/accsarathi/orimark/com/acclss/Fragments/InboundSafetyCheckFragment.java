package accsarathi.orimark.com.acclss.Fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import accsarathi.orimark.com.acclss.Activities.HomeslidingActivity;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.MyUrl;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;
import accsarathi.orimark.com.acclss.Util.ServiceHandler;


public class InboundSafetyCheckFragment extends Fragment {

    View root;
    ImageView header_image;
    TextView header_title;
    RelativeLayout imageView_containor;
    FragmentManager fragmentManager;
    Button bulker_check_btn;
    EditText v_pass_edt,d_pass_edt;
    NetworkConnection nw;
    ProgressDialog prgDialog;
    Boolean netConnection = false;
    String v_pass_no,d_pass_no;
    public static InboundSafetyCheckFragment newInstance() {
        return new InboundSafetyCheckFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = null;
        root = inflater.inflate(R.layout.bulker_safety_check_page, container, false);
        nw = new NetworkConnection(getActivity());
        prgDialog = new ProgressDialog(getActivity());
        prgDialog.setCancelable(false);
        header_title = (TextView)root.findViewById(R.id.title);
        imageView_containor = (RelativeLayout) root.findViewById(R.id.imageView_containor);
        bulker_check_btn = (Button)root.findViewById(R.id.bulker_check_btn);
        header_title.setText("Inbound Safety Check");
        header_image =(ImageView)root.findViewById(R.id.title_img);
        imageView_containor.setVisibility(View.VISIBLE);
        header_image.setImageResource(R.drawable.inbound_vehicle);
        HomeslidingActivity.menu_icon.setBackgroundResource(R.drawable.left_arrow_50x50);

        v_pass_edt = root.findViewById(R.id.v_pass_edt);
        d_pass_edt = root.findViewById(R.id.d_pass_edt);
       // v_pass_edt.setText("VPNO123456");
       // d_pass_edt.setText("DPNO123456");

        HomeslidingActivity.menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectItem(10);
            }
        });
        bulker_check_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                v_pass_no = v_pass_edt.getText().toString();
                d_pass_no = d_pass_edt.getText().toString();
                if(v_pass_no.isEmpty()){
                    v_pass_edt.setError("Enter vehicle passport number");
                }if(d_pass_no.isEmpty()){
                    d_pass_edt.setError("Enter driver passport number");
                }else{

                    if (nw.isConnectingToInternet()) {
                        new downloadTaskOperation().execute();
                    } else {

                        Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                    }
                    //selectItem(1);
                }

            }
        });
        return root;
    }
    public void selectItem(int position) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (position) {
            case 1:
                fragmentClass = InboundVehicleCheckListFragment.class;
                break;
            default:
                fragmentClass = SafetyAuditFragment.class;
        }
        if (fragmentClass != null) {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }

    private class downloadTaskOperation extends AsyncTask<String, Void, Void> {

        String status, message;
        JSONObject json2=null;
        String response;
        HashMap<String,String> all_locations = new HashMap<>();
        ArrayList<String> all_loc = new ArrayList<>();
        @Override
        protected void onPreExecute() {
            prgDialog.setMessage(getResources().getString(R.string.otp_verifiy));
            prgDialog.show();
        }

        @Override
        protected Void doInBackground(String... urls) {

            if (nw.isConnectingToInternet() == true) {
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    ServiceHandler sh = new ServiceHandler();
                    nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.action, "acclsscheckvd"));
                    nameValuePairs.add(new BasicNameValuePair("dpassportno", d_pass_no));
                    nameValuePairs.add(new BasicNameValuePair("vpassportno", v_pass_no));

                    response = sh.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST,
                            nameValuePairs);
                    Log.e("response", response);

                    JSONObject js = new JSONObject(response);
                    json2 = js.getJSONObject(AllStaticVariables.data);
                    status = json2.getString(AllStaticVariables.status);
                    message = json2.getString(AllStaticVariables.msg);


                    /*checklistserialno,userid,vehicleid,driverid,vehicleregdno,drivername,driverdlno,drivermobileno,transportername,transporterid,plantlocationid,plantid,fcomment,chkliststatus,alllowesclatestatus,radioanswer,radioquestionID,radioquestioncomments
*/

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                netConnection = true;
            } else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();

            if (netConnection == false) {
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                if (status.equalsIgnoreCase("1")) {
                    try {
                        MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.vehicleid, json2.getString("vehicleid"));
                        MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.driverid, json2.getString("driverid"));
                        MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.vehicleregdno, json2.getString("vehicleregdno"));
                        MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.payloadcapacity, json2.getString("payloadcapacity"));
                        MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.drivername, json2.getString("drivername"));
                        MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.driverdlno, json2.getString("driverdlno"));
                        MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.drivermobileno, json2.getString("drivermobileno"));
                        MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.transportername, json2.getString("transportername"));
                        MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.transporterid, json2.getString("transporterid"));
                        MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.transportcontractorsupervisor, json2.getString("transportcontractorsupervisor"));
                        MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.drvrpassportno, json2.getString("drvrpassportno"));
                        MyUrl.saveDataWithKeyAndValue(getActivity(), AllStaticVariables.mySharedpreference.vpassportno, json2.getString("vpassportno"));

                        selectItem(1);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }else{
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                }
            }
            super.onPostExecute(result);
        }

    }

}
