package accsarathi.orimark.com.acclss.Activities;

import android.content.Intent;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.ApiHelper;
import accsarathi.orimark.com.acclss.Util.MyUrl;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;

public class LoginActivity extends AppCompatActivity {
    Button loginbtn;
    EditText login_number,login_password;
    NetworkConnection nw;
    String unique_id;
    TextView forgtpswd;
    TextInputLayout inputlayout_mobile,inputlayout_password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //internet
        nw = new NetworkConnection(LoginActivity.this);
        loginbtn = findViewById(R.id.loginbtn);
        login_number = findViewById(R.id.login_number);
        login_password = findViewById(R.id.login_password);
        forgtpswd = findViewById(R.id.forgtpswd);
        unique_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        MyUrl.saveDataWithKeyAndValue(this, "device_id", unique_id);
        System.out.println("unique_id=======" + unique_id);

        inputlayout_mobile = (TextInputLayout) findViewById(R.id.login_input_mobile);
        inputlayout_password = (TextInputLayout) findViewById(R.id.login_input_password);


        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginform();
            }
        });

        forgtpswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent homeIntent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(homeIntent);
            }
        });


    }
    private void loginform() {

        String loginNumber = login_number.getText().toString();
        String loginPassword = login_password.getText().toString();

        if (!validateMobile()) {
            return;
        }
     /*   if (!validatePassword()) {
            return;
        }*/
        if (nw.isConnectingToInternet()) {
            String token_ = MyUrl.getDataFromKey(LoginActivity.this,AllStaticVariables.FIREBASE_TOKEN);
            new ApiHelper(LoginActivity.this, AllStaticVariables.apiAction.signin,
                    loginNumber, unique_id, token_,signInListener).execute();
        } else {

            Toast.makeText(LoginActivity.this, getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        }
    }
    private boolean validateMobile() {
        String valphone = login_number.getText().toString().trim();
        if (valphone.isEmpty()) {
            inputlayout_mobile.setError(getString(R.string.err_msg_phone));
            requestFocus(login_number);
            return false;
        } else if (!isValidPhoneNumber(valphone)) {
            inputlayout_mobile.setError(getString(R.string.err_msg_phone));
            requestFocus(login_number);
            return false;
        }else {
            inputlayout_mobile.setErrorEnabled(false);
        }

        return true;
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    public static boolean isValidPhoneNumber(String phone) {
        boolean check;
        if (phone.length() < 10 || phone.length() > 12) {
            check = false;
        } else {
            check = true;
        }
        return check;
    }
    private boolean validatePassword() {
       String valpassword = login_password.getText().toString().trim();
        if (valpassword.isEmpty()) {
            inputlayout_password.setError(getString(R.string.err_msg_password));
            requestFocus(login_password);
            return false;
        } else if ((valpassword.length() < 6)) {
            inputlayout_password.setError(getString(R.string.err_msg_password_strength));
            requestFocus(login_password);
            return false;
        }else {
            inputlayout_password.setErrorEnabled(false);
        }
        return true;
    }
    private ApiHelper.TaskDelegate signInListener = new ApiHelper.TaskDelegate() {
        public void onTaskFisnishGettingData(Object result) {
            try {
                if (result != null) {
                    JSONObject json2;
                    JSONObject json = (JSONObject) result;
                    json2 = json.getJSONObject(AllStaticVariables.data);
                    int status = json2.getInt(AllStaticVariables.status);
                    String message = json2.getString(AllStaticVariables.msg);
                    System.out.println("######################status:" + status);

                    if (status == 1) {
                        MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.userid, json2.getString("userid"));
                        MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.login_code, json2.getString("login_code"));
                        MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.username, json2.getString("name"));
                        MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.useremail, json2.getString("email"));
                        MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.mobno, json2.getString("mobile"));
                        MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.lssrole, json2.getString("lssrole"));
                        MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.lssroleid, json2.getString("lssroleid"));
                        MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.pwd, login_password.getText().toString());
                        MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.plantid, json2.getString("plantid"));
                        MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.escbuttonshow, json2.getString("escbuttonshow"));


                        /*if (json2.getString("logo").equalsIgnoreCase("") || json2.getString("logo").isEmpty()) {
                            MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.profile_pic, AllStaticVariables.NA);
                        } else {
                            MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.profile_pic, imagepath + json2.getString("logo"));
                        }*/
                        //Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Intent homeIntent = new Intent(LoginActivity.this, OTPActivity.class);
                        finish();
                        startActivity(homeIntent);
                    } else {

                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    };

}
