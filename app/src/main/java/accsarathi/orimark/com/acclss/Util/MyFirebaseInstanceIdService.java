package accsarathi.orimark.com.acclss.Util;

/**
 * Created by oditek on 05/02/18.
 */

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Belal on 12/8/2017.
 */

//the class extending FirebaseInstanceIdService
public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {


    //this method will be called
    //when the token is generated
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        //now we will have the token
        String token = FirebaseInstanceId.getInstance().getToken();

        Log.d("MyRefreshedToken", token);

        //AllStaticVariables.FIREBASE_TOKEN = token;
        sendRegistrationToServer(token);
    }
    private void sendRegistrationToServer(String token) {
        MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.FIREBASE_TOKEN, token);
    }
}
