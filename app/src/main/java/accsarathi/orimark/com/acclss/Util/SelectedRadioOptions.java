package accsarathi.orimark.com.acclss.Util;

/**
 * Created by prakash on 01/08/17.
 */

public class SelectedRadioOptions {

    public String getRadioAnsId() {
        return radioAnsId;
    }

    public void setRadioAnsId(String radioAnsId) {
        this.radioAnsId = radioAnsId;
    }

    public String getRadioQstId() {
        return radioQstId;
    }

    public void setRadioQstId(String radioQstId) {
        this.radioQstId = radioQstId;
    }

    private String radioAnsId;
    private String radioQstId;

    public SelectedRadioOptions(String radioQstId, String radioAnsId){
        this.radioQstId=radioQstId;
        this.radioAnsId=radioAnsId;

    }


}
