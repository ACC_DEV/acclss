package accsarathi.orimark.com.acclss.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import accsarathi.orimark.com.acclss.Activities.HomeslidingActivity;
import accsarathi.orimark.com.acclss.Activities.VechcicleMovementDetails;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.ApiHelper;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;


public class VehicleMovementFragment extends Fragment {

    View root;

    ImageView header_image;
    TextView header_title;
    RelativeLayout imageView_containor;
    FragmentManager fragmentManager;
    private Button btn_check_vechpsprt;
    private EditText v_pass_edt;
    private String v_pass_no;
    private NetworkConnection networkConnection;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = null;
        root = inflater.inflate(R.layout.vehicle_movement_page, container, false);
        networkConnection = new NetworkConnection(getActivity());
        InitResources();
        initback();
        return root;
    }

    private void initback() {
        HomeslidingActivity.menu_icon.setBackgroundResource(R.drawable.left_arrow_50x50);
        HomeslidingActivity.menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HomeslidingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });
    }

    private void InitResources() {
        header_title = root.findViewById(R.id.title);
        header_title.setText("Vehicle Movement");
        header_image = root.findViewById(R.id.title_img);
        imageView_containor = root.findViewById(R.id.imageView_containor);
        imageView_containor.setVisibility(View.VISIBLE);
        header_image.setImageResource(R.drawable.vehicle_movement);

        v_pass_edt = root.findViewById(R.id.v_pass_edt);
        btn_check_vechpsprt = root.findViewById(R.id.btn_check_vechpsprt);
        btnListener();
    }

    private void btnListener() {
        btn_check_vechpsprt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                v_pass_no = v_pass_edt.getText().toString();
                if (v_pass_no.isEmpty()) {
                    v_pass_edt.setError("Enter vehicle passport number");
                } else {
                    if (networkConnection.isConnectingToInternet()) {
                        new ApiHelper(getActivity(), AllStaticVariables.apiAction.chkVechiclePssprt, v_pass_no, passportListener).execute();
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
    }

    private ApiHelper.TaskDelegate passportListener = new ApiHelper.TaskDelegate() {
        public void onTaskFisnishGettingData(Object result) {
            try {
                if (result != null) {
                    JSONObject json = (JSONObject) result;
                    int status = json.getInt("status");
                    if (status == 0) {
                        String msg = json.getString("msg");
                        Toast toast = Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else {
                        JSONArray dataArray = json.getJSONArray("data");
                        Intent detail_vechicle = new Intent(getActivity(), VechcicleMovementDetails.class);
                        detail_vechicle.putExtra("jsonArray", dataArray.toString());
                        detail_vechicle.putExtra("vregistrationno", json.getString("vregistrationno"));
                        detail_vechicle.putExtra("drivername", json.getString("drivername"));
                        detail_vechicle.putExtra("checklist_id", json.getString("checklist_id"));
                        detail_vechicle.putExtra("currentstatus", json.getString("currentstatus"));
                        detail_vechicle.putExtra("currentstatusid", json.getString("currentstatusid"));
                        getActivity().startActivity(detail_vechicle);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void selectItem(int position) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (position) {

            default:
                fragmentClass = LandingFragment.class;
        }
        if (fragmentClass != null) {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


}
