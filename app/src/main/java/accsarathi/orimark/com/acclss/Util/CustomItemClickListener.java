package accsarathi.orimark.com.acclss.Util;

import android.view.View;

/**
 * Created by star on 3/1/2018.
 */

public interface CustomItemClickListener {
    public void onItemClick(View v, int position);
}