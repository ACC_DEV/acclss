package accsarathi.orimark.com.acclss.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import com.squareup.picasso.Picasso;

import accsarathi.orimark.com.acclss.Fragments.AllowFragment;
import accsarathi.orimark.com.acclss.Fragments.LandingFragment;
import accsarathi.orimark.com.acclss.Fragments.OperatorLandingFragment;
import accsarathi.orimark.com.acclss.Fragments.RejectedFragment;
import accsarathi.orimark.com.acclss.Model.DecisionPending;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.CustomTypefaceSpan;
import accsarathi.orimark.com.acclss.Util.MyUrl;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeslidingActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private String profilename, profileEmail;
    FragmentManager fragmentManager;
    Typeface typeFaceBold, typeFaceRegular;
    private RelativeLayout notoRelative;
    NetworkConnection nw;
    private String logincodecheckStatus;
    public static ImageButton menu_icon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_sliding_main);
        nw=new NetworkConnection(this);

        typeFaceBold = Typeface.createFromAsset(this.getAssets(), "fonts/PTC75F_bold.ttf");
        typeFaceRegular = Typeface.createFromAsset(this.getAssets(), "fonts/PTC55F_regular.ttf");


        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);
            applyFontToMenuItem(mi);
        }

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        profilename = MyUrl.getDataFromKey(this, AllStaticVariables.mySharedpreference.username);
        profileEmail = MyUrl.getDataFromKey(this, AllStaticVariables.mySharedpreference.useremail);
        String loginCode = MyUrl.getDataFromKey(this, AllStaticVariables.mySharedpreference.login_code);
        String userId = MyUrl.getDataFromKey(this, AllStaticVariables.mySharedpreference.userid);
        menu_icon = (ImageButton)findViewById(R.id.menubar);
        menu_icon.setBackgroundResource(R.mipmap.menu_bar);
        setupDrawerContent(navigationView);

        Bundle b = getIntent().getExtras();
        if (b != null) {

            String type = getIntent().getStringExtra("type");
              selectItem(Integer.valueOf(type));

          }else {

              if (savedInstanceState == null) {
                  Fragment fragment = null;
                  Class fragmentClass = null;
                  fragmentClass = OperatorLandingFragment.class;
                  try {
                      fragment = (Fragment) fragmentClass.newInstance();
                  } catch (Exception e) {
                      e.printStackTrace();
                  }

                  fragmentManager = getSupportFragmentManager();
                  fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
              }
          }



        View header = navigationView.getHeaderView(0);
        TextView tv_email = (TextView) header.findViewById(R.id.tv_email);
        TextView tv_name = (TextView) header.findViewById(R.id.header_name);
        tv_name.setText(profilename);
        tv_email.setText(profileEmail);
        tv_name.setTypeface(typeFaceBold);
        tv_email.setTypeface(typeFaceBold);
        CircleImageView circleImageView = (CircleImageView) header.findViewById(R.id.imageView);
        String profilePic = MyUrl.getDataFromKey(this, AllStaticVariables.mySharedpreference.profile_pic);
        if (AllStaticVariables.NA.equalsIgnoreCase(profilePic)) {
            Picasso.with(this).load(R.drawable.user_profile_inactive).into(circleImageView);
        } else {
            Glide
                    .with(this)
                    .load(profilePic)
                    .error(R.drawable.user_profile_inactive)
                    .centerCrop()
                    .crossFade()
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(circleImageView);
        }


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });

    }

    private void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , typeFaceBold), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public void slideMenuBtnAction(View v) {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            drawerLayout.openDrawer(GravityCompat.END);
        }
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (menuItem.getItemId()) {
            case R.id.home:
                fragmentClass = LandingFragment.class;
                break;

            case R.id.logout:
                drawerLayout.closeDrawers();
                showLogoutWarning();
                break;
            default:
               // fragmentClass = LandingFragment.class;
        }
        if (fragmentClass != null) {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

                menuItem.setChecked(true);
                //setTitle(menuItem.getTitle());


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        drawerLayout.closeDrawers();

    }

    private void showLogoutWarning() {
        android.app.AlertDialog.Builder builder1;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder1 = new android.app.AlertDialog.Builder(HomeslidingActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder1 = new android.app.AlertDialog.Builder(HomeslidingActivity.this);
        }
        builder1.setMessage(getString(R.string.dialog_logot));
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        logout();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        android.app.AlertDialog alert11 = builder1.create();
        alert11.show();
    }



   @Override
    public void onBackPressed() {

        /*if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/
    }



    private void logout() {
        MyUrl.saveDataWithKeyAndValue(getApplicationContext(), AllStaticVariables.mySharedpreference.isLogin, null);
        Intent i = new Intent(HomeslidingActivity.this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
        android.os.Process.killProcess(android.os.Process.myPid());
    }
    public void selectItem(int position) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (position) {
            case 0:
                fragmentClass = RejectedFragment.class;
                break;
            case 1:
                fragmentClass = AllowFragment.class;
                break;

            case 2:
                fragmentClass = DecisionPending.class;
                break;

            default:
                // fragmentClass = LandingFragment.class;
        }
        if (fragmentClass != null) {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }

}
