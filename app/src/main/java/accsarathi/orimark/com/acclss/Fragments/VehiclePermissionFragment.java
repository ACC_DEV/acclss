package accsarathi.orimark.com.acclss.Fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import accsarathi.orimark.com.acclss.Activities.HomeslidingActivity;
import accsarathi.orimark.com.acclss.R;
import accsarathi.orimark.com.acclss.Util.AllStaticVariables;
import accsarathi.orimark.com.acclss.Util.MyUrl;
import accsarathi.orimark.com.acclss.Util.NetworkConnection;
import accsarathi.orimark.com.acclss.Util.ServiceHandler;


public class VehiclePermissionFragment extends Fragment {

    View root;
    ImageView header_image;
    TextView header_title;
    RelativeLayout imageView_containor;
    FragmentManager fragmentManager;
    NetworkConnection nw;
    ProgressDialog prgDialog;
    Boolean netConnection = false;
    String plantid,checklistserialno,formtype,escalatedtoid,escalatedto,lss_rolll,name,alllowesclatestatus,user_id,location_name,location_id,vehicleid,driverid,vehicleregdno,payloadcapacity,drivername,driverdlno,drivermobileno,transportername,transporterid,transportcontractorsupervisor;
    EditText lss_role_edt,name_edt,driver_name_edt,location_edt,escalted_to,escalated_by,lss_role,date_edt,final_comment;
    Button sumbtn;
    public static VehiclePermissionFragment newInstance() {
        return new VehiclePermissionFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = null;
        root = inflater.inflate(R.layout.vehicle_permission, container, false);
        nw = new NetworkConnection(getActivity());
        prgDialog = new ProgressDialog(getActivity());
        prgDialog.setCancelable(false);

        header_title = (TextView)root.findViewById(R.id.title);
        imageView_containor = (RelativeLayout) root.findViewById(R.id.imageView_containor);
        header_title.setText("Vehicle Escalation");
        header_image =(ImageView)root.findViewById(R.id.title_img);
        imageView_containor.setVisibility(View.VISIBLE);
        header_image.setImageResource(R.drawable.vehicle);

        name_edt = (EditText)root.findViewById(R.id.name_edt);
        driver_name_edt = (EditText)root.findViewById(R.id.driver_name_edt);
        location_edt = (EditText)root.findViewById(R.id.location_edt);
        escalted_to = (EditText)root.findViewById(R.id.escalted_to);
        escalated_by = (EditText)root.findViewById(R.id.escalated_by);
        lss_role_edt = (EditText)root.findViewById(R.id.lss_role);
        date_edt = (EditText)root.findViewById(R.id.date_edt);
        final_comment = (EditText)root.findViewById(R.id.cooment);
        sumbtn =(Button)root.findViewById(R.id.sumbtn);

        name = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.username);

        vehicleid = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.vehicleid);
        driverid = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.driverid);
        vehicleregdno = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.vehicleregdno);
        payloadcapacity = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.payloadcapacity);
        drivername = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.drivername);
        driverdlno = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.driverdlno);
        drivermobileno = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.drivermobileno);
        transportername = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.transportername);
        transporterid = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.transporterid);
        transportcontractorsupervisor = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.transportcontractorsupervisor);
        location_name = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.selected_location);
        location_id = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.selected_location_id);
        user_id = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.userid);
        lss_rolll = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.lssrole);
        escalatedto = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.escalatedto);
        escalatedtoid = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.escalatedtoid);
        formtype = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.formtype);
        checklistserialno = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.checklistserialno);
        plantid = MyUrl.getDataFromKey(getActivity(), AllStaticVariables.mySharedpreference.plantid);


        name_edt.setText(vehicleregdno);
        driver_name_edt.setText(drivername);
        location_edt.setText(location_name);
        escalated_by.setText(name);
        escalted_to.setText(escalatedto);
        lss_role_edt.setText(lss_rolll);
        date_edt.setText(currentTime());
        HomeslidingActivity.menu_icon.setBackgroundResource(R.drawable.left_arrow_50x50);
        HomeslidingActivity.menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectItem(10);
            }
        });
        sumbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String final_comme= final_comment.getText().toString();
                if(final_comme.trim().length()==0){
                    final_comment.setError("Please write your comment");
                }else {
                    new downloadReOtpOperation().execute();
                }


            }
        });
        return root;
    }

    public String currentTime(){

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh.mma");
        Date date = new Date();
        return dateFormat.format(date); //2016/11/16 12:08:43
    }
    public void selectItem(int position) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (position) {

            default:
                fragmentClass = SafetyAuditFragment.class;
        }
        if (fragmentClass != null) {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }


    private class downloadReOtpOperation extends AsyncTask<String, Void, Void> {

        String status, message;
        JSONObject json2;
        String response;

        @Override
        protected void onPreExecute() {
            prgDialog.setMessage("Sending...please wait.");
            prgDialog.show();
        }

        @Override
        protected Void doInBackground(String... urls) {

            if (nw.isConnectingToInternet() == true) {
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    ServiceHandler sh = new ServiceHandler();

                    nameValuePairs.add(new BasicNameValuePair(AllStaticVariables.action, "acclssEscalateVehicleFormSubmit"));
                    nameValuePairs.add(new BasicNameValuePair("checklist_id", checklistserialno));
                    nameValuePairs.add(new BasicNameValuePair("userid", user_id));
                    nameValuePairs.add(new BasicNameValuePair("vehicleid", vehicleid));
                    nameValuePairs.add(new BasicNameValuePair("driverid", driverid));
                    nameValuePairs.add(new BasicNameValuePair("vehicleregdno", vehicleregdno));
                    nameValuePairs.add(new BasicNameValuePair("drivername", drivername));

                    nameValuePairs.add(new BasicNameValuePair("plantlocationid", location_id));
                    nameValuePairs.add(new BasicNameValuePair("plantid", plantid));
                    nameValuePairs.add(new BasicNameValuePair("comments", final_comment.getText().toString()));

                    nameValuePairs.add(new BasicNameValuePair("allowed", name));
                    nameValuePairs.add(new BasicNameValuePair("formtype", formtype));
                    nameValuePairs.add(new BasicNameValuePair("lssrole",lss_rolll));
                    nameValuePairs.add(new BasicNameValuePair("escalateby", name));
                    nameValuePairs.add(new BasicNameValuePair("escalatedtoid", escalatedtoid));
                    nameValuePairs.add(new BasicNameValuePair("escalatedto", escalatedto));


                   /* userid,checklist_id,vehicleid,driverid,vehicleregdno,drivername,plantlocationid,plantid,allowed,escalatedto,escalatedtoid,escalateby,lssrole,formtype,comments
                   */ response = sh.makeServiceCall(MyUrl.baseAPI, ServiceHandler.POST,
                            nameValuePairs);
                    Log.e("response", response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString(AllStaticVariables.status);
                    Log.e("status", status);

                    if ("0".equalsIgnoreCase(status)) {
                        message = js.getString(AllStaticVariables.msg);
                    } else {
                        message = js.getString(AllStaticVariables.msg);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                netConnection = true;
            } else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();

            if (netConnection == false) {
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                if (status.equalsIgnoreCase("1")) {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();


                        Thread timerThread = new Thread() {
                            public void run() {
                                try {
                                    sleep(2000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } finally {
                                    selectItem(10);
                                }
                            }
                        };
                        timerThread.start();


                } else {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
            super.onPostExecute(result);
        }

    }
}
