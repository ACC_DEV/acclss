package accsarathi.orimark.com.acclss.Model;

/**
 * Created by prakash on 1/31/18.
 */

public class RejectedModel {
    public String getVpassportno() {
        return vpassportno;
    }

    public void setVpassportno(String vpassportno) {
        this.vpassportno = vpassportno;
    }

    public String getDrvrpassportno() {
        return drvrpassportno;
    }

    public void setDrvrpassportno(String drvrpassportno) {
        this.drvrpassportno = drvrpassportno;
    }

    public String getInspectionstatus() {
        return inspectionstatus;
    }

    public void setInspectionstatus(String inspectionstatus) {
        this.inspectionstatus = inspectionstatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChecklist_id() {
        return checklist_id;
    }

    public void setChecklist_id(String checklist_id) {
        this.checklist_id = checklist_id;
    }

    public String getVehicleregdno() {
        return vehicleregdno;
    }

    public void setVehicleregdno(String vehicleregdno) {
        this.vehicleregdno = vehicleregdno;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAuditedby() {
        return auditedby;
    }

    public void setAuditedby(String auditedby) {
        this.auditedby = auditedby;
    }

    public String getLssrole() {
        return lssrole;
    }

    public void setLssrole(String lssrole) {
        this.lssrole = lssrole;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    private String id;
    private String checklist_id;
    private String vehicleregdno;
    private String location;
    private String auditedby;
    private String lssrole;
    private String datetime;
    private String status;

    private String vpassportno;
    private String drvrpassportno;
    private String inspectionstatus;

    public String getFormtype() {
        return formtype;
    }

    public void setFormtype(String formtype) {
        this.formtype = formtype;
    }

    private String formtype;

    public RejectedModel(String id, String checklist_id, String vehicleregdno, String location, String auditedby,
                         String lssrole, String datetime, String status,String formtype,String vpassportno, String drvrpassportno,String inspectionstatus) {
        this.id = id;
        this.checklist_id = checklist_id;
        this.vehicleregdno = vehicleregdno;
        this.location = location;
        this.auditedby = auditedby;
        this.lssrole = lssrole;
        this.datetime = datetime;
        this.status = status;
        this.formtype = formtype;

        this.vpassportno = vpassportno;
        this.drvrpassportno = drvrpassportno;
        this.inspectionstatus = inspectionstatus;

    }
}
